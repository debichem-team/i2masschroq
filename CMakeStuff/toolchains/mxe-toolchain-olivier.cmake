# File:///home/langella/developpement/git/pappsomspp/CMakeStuff/toolchains/mxe-toolchain.cmake# 
# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

message("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

# The TARGET changes according to the plaform
# For example, it changes to i2MassChroQ for macOS.

SET(TARGET i2masschroq)

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /win64/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /win64/mxe/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

message(STATUS "${BoldGreen}Setting definition -DPMSPP_LIBRARY for symbol DLL export.${ColourReset}")
add_definitions(-DPMSPP_LIBRARY)


find_package(ZLIB REQUIRED)

find_package(SQLite3 REQUIRED)


#install MXE qtsvg package
#langella@piccolo:/media/langella/pappso/mxe$ make qtsvg



set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIR "/win64/qcustomplot-2.0.1+dfsg1")
set(QCustomPlot_LIBRARIES "/win64/qcustomplot-2.0.1+dfsg1/wbuild/libqcustomplot.dll")
if(NOT TARGET QCustomPlot::QCustomPlot)
	add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
	set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
		IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIR}"
		INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


set(Quazip5_FOUND 1)
set(Quazip5_INCLUDE_DIRS "/win64/libquazip-0.7.6")
set(Quazip5_LIBRARY "/win64/libquazip-0.7.6/build/libquazip5.dll") 
if(NOT TARGET Quazip5::Quazip5)
    add_library(Quazip5::Quazip5 UNKNOWN IMPORTED)
    set_target_properties(Quazip5::Quazip5 PROPERTIES
        IMPORTED_LOCATION             "${Quazip5_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${Quazip5_INCLUDE_DIRS}")
endif()



set(OdsStream_FOUND 1)
set(OdsStream_INCLUDE_DIRS "/home/langella/developpement/git/libodsstream/src")
set(OdsStream_LIBRARY "/home/langella/developpement/git/libodsstream/wbuild/src/libodsstream.dll")
if(NOT TARGET OdsStream::Core)
    add_library(OdsStream::Core UNKNOWN IMPORTED)
    set_target_properties(OdsStream::Core PROPERTIES
        IMPORTED_LOCATION             "${OdsStream_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${OdsStream_INCLUDE_DIRS}"
    )
endif()


set(Alglib_FOUND 1)
set(Alglib_INCLUDE_DIR "/win64/alglib/src")
set(Alglib_LIBRARY "/win64/alglib/wbuild/libalglib.dll") 
if(NOT TARGET Alglib::Alglib)
	add_library(Alglib::Alglib UNKNOWN IMPORTED)
	set_target_properties(Alglib::Alglib PROPERTIES
		IMPORTED_LOCATION             "${Alglib_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Alglib_INCLUDE_DIR}")
endif()

# All this belly dance does not seem necessary. Just perform like for the other
# libraries...
# Look for the necessary header
set(Zstd_INCLUDE_DIR /mxe/usr/x86_64-w64-mingw32.shared/include)
mark_as_advanced(Zstd_INCLUDE_DIR)
set(Zstd_INCLUDE_DIRS ${Zstd_INCLUDE_DIR})
# Look for the necessary library
set(Zstd_LIBRARY /mxe/usr/x86_64-w64-mingw32.shared/bin/libzstd.dll)
mark_as_advanced(Zstd_LIBRARY)
# Mark the lib as found
set(Zstd_FOUND 1)
set(Zstd_LIBRARIES ${Zstd_LIBRARY})
if(NOT TARGET Zstd::Zstd)
	add_library(Zstd::Zstd UNKNOWN IMPORTED)
	set_target_properties(Zstd::Zstd PROPERTIES
		IMPORTED_LOCATION             "${Zstd_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIR}")
endif()





set(PappsoMSpp_INCLUDE_DIR /home/langella/developpement/git/pappsomspp/src/)
mark_as_advanced(PappsoMSpp_INCLUDE_DIR)
set(PappsoMSpp_INCLUDE_DIRS ${PappsoMSpp_INCLUDE_DIR})
# Look for the necessary library
set(PappsoMSpp_LIBRARY /home/langella/developpement/git/pappsomspp/wbuild/src/libpappsomspp.dll)
mark_as_advanced(PappsoMSpp_LIBRARY)
# Mark the lib as found
set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_LIBRARIES ${PappsoMSpp_LIBRARY})
if(NOT TARGET PappsoMSpp::Core)
    add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Core PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()
set(PappsoMSppWidget_LIBRARY /home/langella/developpement/git/pappsomspp/wbuild/src/pappsomspp/widget/libpappsomspp-widget.dll)
mark_as_advanced(PappsoMSppWidget_LIBRARY)  
message(STATUS "~~~~~~~~~~~~~ ${PappsoMSppWidget_LIBRARY} ~~~~~~~~~~~~~~~")
set(PappsoMSppWidget_FOUND TRUE)
if(NOT TARGET PappsoMSpp::Widget)
    add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
    set_target_properties(PappsoMSpp::Widget PROPERTIES
        IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIRS}")
endif()



set(RData_FOUND 1)
set(RData_INCLUDE_DIR "/win64/librdata/src")
set(RData_LIBRARY "/win64/librdata/.libs/librdata-0.dll") 
if(NOT TARGET RData::Core)
	add_library(RData::Core UNKNOWN IMPORTED)
	set_target_properties(RData::Core PROPERTIES
		IMPORTED_LOCATION             "${RData_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${RData_INCLUDE_DIR}")
endif()

# add grantlee5
# Target "i2masschroq" links to target "Grantlee5::Templates"
# make grantlee
# cp /win64/mxe/usr/x86_64-w64-mingw32.shared/bin/libGrantlee_Templates.dll /win64/mxe_dll
# cp /win64/mxe/usr/x86_64-w64-mingw32.shared/bin/libGrantlee_TextDocument.dll /win64/mxe_dll

set(Grantlee5_INCLUDE_DIR /win64/mxe/usr/x86_64-w64-mingw32.shared/include)
mark_as_advanced(Grantlee5_INCLUDE_DIR)
set(Grantlee5_INCLUDE_DIRS ${Grantlee5_INCLUDE_DIR})
# Look for the necessary library
set(Grantlee5_LIBRARY /win64/mxe/usr/x86_64-w64-mingw32.shared/bin/libGrantlee_Templates.dll)
mark_as_advanced(Grantlee5_LIBRARY)
# Mark the lib as found
set(Grantlee5_FOUND 1)
set(Grantlee5_LIBRARIES ${Grantlee5_LIBRARY})
if(NOT TARGET Grantlee5::Templates)
	add_library(Grantlee5::Templates UNKNOWN IMPORTED)
	set_target_properties(Grantlee5::Templates PROPERTIES
		IMPORTED_LOCATION             "${Grantlee5_LIBRARY}"
		INTERFACE_INCLUDE_DIRECTORIES "${Grantlee5_INCLUDE_DIR}")
endif()

