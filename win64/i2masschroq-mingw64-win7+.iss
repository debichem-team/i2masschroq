[Setup]
AppName=i2MassChroQ

; Set version number below
#define public version "0.4.59"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\i2masschroq"
#define cmake_build_dir "C:\msys64\home\polipo\devel\i2masschroq\build"

; Set version number below
AppVerName=i2MassChroQ version {#version}
DefaultDirName={pf}\i2MassChroQ
DefaultGroupName=xtandempipeline
OutputDir="C:\msys64\home\polipo\devel\i2masschroq\win64"

; Set version number below
OutputBaseFilename=i2masschroq-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=i2masschroq-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2016- Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="i2MassChroQ, by Olivier Langella"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
; WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "C:\i2masschroq-libdeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\libodsstream\build\src\libodsstream-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\libpappsomspp-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\pappsomspp\widget\libpappsomspp-widget-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: i2MassChroQComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: i2MassChroQComp
Source: "{#sourceDir}\win64\i2MassChroQ_icon.ico"; DestDir: {app}; Components: i2MassChroQComp

Source: "{#cmake_build_dir}\src\i2MassChroQ.exe"; DestDir: {app}; Components: i2MassChroQComp 

[Icons]
Name: "{group}\i2MassChroQ"; Filename: "{app}\i2MassChroQ.exe"; WorkingDir: "{app}";IconFilename: "{app}\i2MassChroQ_icon.ico"
Name: "{group}\Uninstall i2MassChroQ"; Filename: "{uninstallexe}"

[Types]
Name: "i2MassChroQType"; Description: "Full installation"

[Components]
Name: "i2MassChroQComp"; Description: "i2MassChroQ files"; Types: i2MassChroQType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\i2MassChroQ.exe"; Description: "Launch i2MassChroQ"; Flags: postinstall nowait unchecked
