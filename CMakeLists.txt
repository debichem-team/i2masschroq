# CMake script for C++ i2MassChroQ
# Author: Olivier Langella
# Created: 05/06/2015
# Use git to check for contributions by other authors.

# Global parameters
CMAKE_MINIMUM_REQUIRED(VERSION 3.0)
PROJECT(i2masschroq CXX)

set(i2MassChroQ_VERSION_MAJOR "0")
set(i2MassChroQ_VERSION_MINOR "4")
set(i2MassChroQ_VERSION_PATCH "59")
set(i2MassChroQ_VERSION "${i2MassChroQ_VERSION_MAJOR}.${i2MassChroQ_VERSION_MINOR}.${i2MassChroQ_VERSION_PATCH}")
set(VERSION 0.4.59)

# Set additional project information
set(COMPANY "pappso.org")
set(COPYRIGHT "Copyright (c) 2022 PAPPSO. Licensed under GPLv3+")
set(IDENTIFIER "org.pappso")


#depending on libpappsomspp version :
set(LIBPAPPSOMSPP_VERSION "0.8.49")
set(LIBRDATA_VERSION "0~20210223+git-85757dc6")


# Add folder where are supportive functions
set(CMAKE_UTILS_PATH ${CMAKE_SOURCE_DIR}/CMakeStuff)
set(CMAKE_TOOLCHAINS_PATH ${CMAKE_UTILS_PATH}/toolchains)
set(CMAKE_MODULE_PATH ${CMAKE_UTILS_PATH}/modules)

# Include the system's uname that fills in SYSTEM_UNAME_S.
# Sets WIN64 if SYSTEM_UNAME_S is "^.*MING64.*"
include(${CMAKE_UTILS_PATH}/systemUname.cmake)

# Include the various colors we want to use in the output
include(${CMAKE_UTILS_PATH}/outputColors.cmake)

set(CMAKE_COLOR_MAKEFILE ON)
set(CMAKE_VERBOSE_MAKEFILE ON)

message("\n${BoldRed}Configuring build for project ${CMAKE_PROJECT_NAME}${ColourReset}\n")

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)


# This export will allow using the flags to be used by
# youcompleteme (vim plugin).
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

if(EXISTS "${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json" )
  execute_process( COMMAND cmake -E copy_if_different
    ${CMAKE_CURRENT_BINARY_DIR}/compile_commands.json
    ${CMAKE_CURRENT_SOURCE_DIR}/compile_commands.json
    )
endif()


# We want C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
message(STATUS "${BoldGreen}CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}${ColourReset}")

#############################################################
# We do not want warnings for unknown pragmas:
message(STATUS "Setting definition -Wno-unknown-pragmas.${ColourReset}")
add_definitions(-Wno-unknown-pragmas)
# Enable warnings and possibly treat them as errors
message(STATUS "${BoldGreen}Setting definition -Wall.${ColourReset}")
add_definitions(-Wall)
message(STATUS "${BoldGreen}Setting definition -Wextra.${ColourReset}")
add_definitions(-Wextra)

if(WARN_AS_ERROR)
  message(STATUS "${BoldYellow}Setting definition -Werror.${ColourReset}")
  add_definitions(-Werror)
endif()

message(STATUS "${BoldRed}CMAKE_SOURCE_DIR: ${CMAKE_SOURCE_DIR}${ColourReset}")


#############################################################
#############################################################
# Platform-specific CMake configuration
if(MXE)
  # Run the following cmake command line:
  # x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ../development
  #include(${CMAKE_TOOLCHAINS_PATH}/mxe-toolchain.cmake)
  include(${CMAKE_TOOLCHAINS_PATH}/mxe-toolchain-olivier.cmake)

  # Set the name to the systemUname variable because in this situation that name
  # is not found, it it passed as a flag in the command line.
  set(systemUname "mxe")

elseif(UNIX AND NOT APPLE)
  # Run the following cmake command line:
  # cmake -DBUILD_USER_MANUAL=1 -DCMAKE_BUILD_TYPE=Debug
  # If the libraries to be used are local and not system-wide add
  # -DLOCAL_DEV=1

  # On UNIX, the libpappsomspp and libpappsomspp-widget libs
  # need to be installed and they ship the PappsoMSppConfig.cmake
  # config file.

  # However, when developing both libs and this program, we need to be able to
  # load the libs from their dev/build directory without relying on the system
  # libs.
  if(LOCAL_DEV)

    # The development should use the locally built libs.
    include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain-local.cmake)
  else()

    # Use the system-wide libraries.
    include(${CMAKE_TOOLCHAINS_PATH}/unix-toolchain.cmake)

  endif()

  message(STATUS "Include dir for PappsoMSpp is at: ${PappsoMSpp_INCLUDE_DIRS}")
  message(STATUS "PappsoMSpp and PappsoMSppWidget were found at:
  \t ${PappsoMSpp_LIBRARIES} and ${PappsoMSppWidget_LIBRARIES}")

elseif(WIN64)
  # Run the following cmake command line:
  # cmake -DBUILD_USER_MANUAL=0 -DCMAKE_BUILD_TYPE=Release ../development

  include(${CMAKE_TOOLCHAINS_PATH}/win10-mingw64-toolchain.cmake)

elseif(APPLE)
  # Run the following cmake command line:
  # cmake -DBUILD_USER_MANUAL=0 -DCMAKE_BUILD_TYPE=Release ../development

  include(${CMAKE_TOOLCHAINS_PATH}/apple-macport-toolchain.cmake)

endif()

message("")
message(STATUS "${BoldGreen}Starting configuration of ${CMAKE_PROJECT_NAME}${ColourReset}")
message("")
message(STATUS "${BoldYellow}The build toolchain is: ${SystemUname}${ColourReset}")
message("")



#############################################################
#############################################################
# Essential software configuration
message(STATUS "CMAKE_CURRENT_BINARY_DIR: " ${CMAKE_CURRENT_BINARY_DIR})

# Gee, do not set that to default!
# set(MAKE_TEST 1)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING
    "Type of build, options are: None, Debug, Release, RelWithDebInfo, MinSizeRel."
    FORCE)
endif(NOT CMAKE_BUILD_TYPE)

if(CMAKE_BUILD_TYPE MATCHES "Release")
  message(STATUS "Compiling in release mode.")
  add_definitions("-DQT_NO_DEBUG_OUTPUT")
endif()

if(CMAKE_BUILD_TYPE MATCHES "Debug")
  message(STATUS "Compiling in debug mode with BUILD_TESTING: ${BUILD_TESTING}.")
  message(STATUS "Add definition -ggdb3 to format debug output for GDB.")
  add_definitions(-ggdb3)
endif()

if(CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo")
  message(STATUS "Compiling in release with debug info mode.")
endif( CMAKE_BUILD_TYPE MATCHES "RelWithDebInfo" )

message(STATUS "${BoldYellow}CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}.${ColourReset}")

# Automate the production of correctly-versioned d/control file.
configure_file (${CMAKE_SOURCE_DIR}/debian/control.cmake.in ${CMAKE_SOURCE_DIR}/debian/control @ONLY)


set(SOFTWARE_NAME "i2MassChroQ")

set(BUILD_SYSTEM_NAME "${CMAKE_SYSTEM_NAME}")

# Set the CMAKE_PREFIX_PATH for the find_library fonction when using non
# standard install location
if(CMAKE_INSTALL_PREFIX)
  set(CMAKE_PREFIX_PATH "${CMAKE_INSTALL_PREFIX}" ${CMAKE_PREFIX_PATH})
endif(CMAKE_INSTALL_PREFIX)

# Subdirectories
add_subdirectory(src)
add_subdirectory(doc)

if(UNIX)

  # Conditional addition of the test directory.
  if(BUILD_TESTING)

    enable_testing()
    include(CTest)
    add_subdirectory(tests)

  endif(BUILD_TESTING)

  # Doxygen
  find_package(Doxygen)
  if(DOXYGEN_FOUND)
    add_custom_target(apidoc cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-build
      COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-build
      COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-build
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
    add_custom_target(apidoc-stable cp Doxyfile ${CMAKE_BINARY_DIR}/Doxyfile-stable
      COMMAND echo "OUTPUT_DIRECTORY=${CMAKE_BINARY_DIR}" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
      COMMAND echo "HTML_HEADER=header.html" >> ${CMAKE_BINARY_DIR}/Doxyfile-stable
      COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile-stable
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
  endif(DOXYGEN_FOUND)
endif(UNIX)
# Packager
set(CPACK_PACKAGE_NAME "i2masschroq")
set(CPACK_PACKAGE_VENDOR "PAPPSO Development Team")
set(CPACK_PACKAGE_VERSION "${i2MassChroQ_VERSION}")
set(CPACK_PACKAGE_VERSION_MAJOR "${i2MassChroQ_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${i2MassChroQ_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${i2MassChroQ_VERSION_PATCH}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "protein inference tool")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
set(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
set(CPACK_RESOURCE_FILE_INSTALL "${CMAKE_SOURCE_DIR}/INSTALL")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES
  "CMakeFiles"
  "Makefile"
  "_CPack_Packages"
  "CMakeCache.txt"
  ".*\\\\.git"
  ".*\\\\.gz"
  ".*\\\\.deb"
  ".*\\\\.rpm"
  ".*\\\\.dmg"
  ".*\\\\.xpip"
  ".*\\\\..*\\\\.swp"
  "src/\\\\..*"
  "src/libbpp*"
  "debian/tmp"
  "debian/libbpp.*/"
  "debian/libbpp.*\\\\.so.*"
  "debian/libbpp.*\\\\.a"
  "debian/libbpp.*\\\\.substvars"
  "debian/libbpp.*\\\\.debhelper"
  "debian/debhelper\\\\.log"
  "build/"
  "cbuild/"
  "wbuild/"
  "Core.tag"
  "Testing"
  "build-stamp"
  "install_manifest.txt"
  "DartConfiguration.tcl"
  ${CPACK_SOURCE_IGNORE_FILES}
  )



#dch -Djessie "message"
#cmake ..
#make deb
# scp i2masschroq* xtpcpp* proteus.ideev.universite-paris-saclay.fr:/var/www/apt/incoming
# reprepro -Vb /var/www/apt processincoming default
#
#  debuild -S -sa
# dput -f olivier-langella *changes

if(MACOS)
  SET(CPACK_GENERATOR "Bundle")
endif()

set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_DEBSOURCE_PACKAGE_FILE_NAME "lib${CMAKE_PROJECT_NAME}_${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}.orig")
iNCLUDE(CPack)

#This adds the 'dist' target
add_custom_target(dist COMMAND ${CMAKE_MAKE_PROGRAM} package_source)
# 'clean' is not (yet) a first class target. However, we need to clean the directories before building the sources:
if("${CMAKE_GENERATOR}" MATCHES "Make")
  add_custom_target(make_clean
    COMMAND ${CMAKE_MAKE_PROGRAM} clean
    WORKING_DIRECTORY ${CMAKE_CURRENT_DIR}
    )
  Add_dependencies(dist make_clean)
endif()

if(UNIX)
  #This creates deb packages:

  add_custom_target(targz
    cpack -G TGZ --config CPackSourceConfig.cmake && tar xvfz ${CPACK_PACKAGE_NAME}-${i2MassChroQ_VERSION}.tar.gz && ln -s ${CPACK_PACKAGE_NAME}-${i2MassChroQ_VERSION}.tar.gz ${CPACK_PACKAGE_NAME}_${i2MassChroQ_VERSION}.orig.tar.gz
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Creating .tar.gz" VERBATIM
    )


  add_custom_target(deb
    cd ${CPACK_PACKAGE_NAME}-${i2MassChroQ_VERSION} && dpkg-buildpackage -B
    DEPENDS targz
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Creating Debian package" VERBATIM
    )

  add_custom_target(deball
    cd ${CPACK_PACKAGE_NAME}-${i2MassChroQ_VERSION} && dpkg-buildpackage
    DEPENDS targz
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Creating Debian package" VERBATIM
    )

  file(GLOB changesdebs "${CMAKE_BINARY_DIR}/${CPACK_PACKAGE_NAME}_*.dsc")

  foreach(libfile ${changesdebs})
    SET(changesdeb "${libfile}")
  endforeach(libfile)

  message ("changes debian file : ${changesdeb}")
  #lintian -IEi --pedantic tandem-mass_2013.06.15-1_amd64.changes
  add_custom_target(lintian
    lintian -IEi --pedantic ${changesdeb}
    DEPENDS deb
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "lintian check" VERBATIM
    )

endif(UNIX)

set(ctest_update_type GIT)
set(update_command "GIT")
set(UPDATE_OPTIONS "")

configure_file (${CMAKE_SOURCE_DIR}/templates/share/applications/i2MassChroQ.desktop.cmake ${CMAKE_BINARY_DIR}/templates/share/applications/i2MassChroQ.desktop)

configure_file (${CMAKE_SOURCE_DIR}/win64/i2masschroq-mingw64-win7+.iss.cmake ${CMAKE_SOURCE_DIR}/win64/i2masschroq-mingw64-win7+.iss)

configure_file (${CMAKE_SOURCE_DIR}/win64/i2masschroq-mxe.iss.cmake ${CMAKE_SOURCE_DIR}/win64/i2masschroq-mxe.iss)

install(FILES ${CMAKE_BINARY_DIR}/templates/share/applications/i2MassChroQ.desktop DESTINATION share/applications)


if(NOT APPLE)

  # We want to install the binary arch-dependent target in
  # specific situations. To have proper control, we define the arch
  # component.
  install(TARGETS ${TARGET}
    RUNTIME
    COMPONENT arch
    DESTINATION ${BIN_DIR})

endif()

configure_file(${CMAKE_SOURCE_DIR}/src/resources/i2MassChroQ_icon.svg
  ${CMAKE_BINARY_DIR}/src/resources/i2MassChroQ.svg COPYONLY)

install(FILES ${CMAKE_BINARY_DIR}/src/resources/i2MassChroQ.svg DESTINATION share/icons/hicolor/scalable/apps)
