<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE preface [

<!ENTITY % entities SYSTEM "pappso-user-manuals.ent">
%entities;

<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">
<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN"
"/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>
<preface xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xml:id="preface" version="5.0">
  <info>
    <title>Preface</title>
    <keywordset>
      <keyword>software features</keyword>
      <keyword>intended audience</keyword>
      <keyword>report feedback</keyword>
    </keywordset>
  </info>

  <sect1>

    <title>Software feature offerings and intended audience</title>

    <para>This manual is about the &i2mcq; protein identification software project.</para>

    <para>&i2mcq; has the following features:</para>

    <itemizedlist>

      <listitem> <para> Load mass spectometry data files in the mzXML or mzML
      format, thanks to the excellent <application>libpwiz</application> library
      of ProteoWizard<footnote><para><link
      xlink:href="http://proteowizard.sourceforge.net/"/>.</para></footnote>
fame. </para> </listitem>

  <listitem>

    <para> 

      Configure the way the peptide/mass spectrum matches (PSM) are to be performed;

    </para>

  </listitem>

  <listitem>

    <para> 

      Configure the database files to be used (target organism databases and
      contaminant databases);

    </para>

  </listitem>

  <listitem>

    <para> 

      Use the MS/MS data in the file to feed the &xtandem; program that produces
      peptide identification results by matching the measured ion masses with
      peptide fragments calculated <emphasis>in silico</emphasis> on the basis
      of the databases contents; 

    </para>

  </listitem>

  <listitem>

    <para>

      Perform the protein inference step that leads to reliable protein
      identifications on the basis of the peptide identifications performed by
      &xtandem;

    </para>

  </listitem>

  <listitem>

    <para> 

      Display the data obtained at any step in powerful ways in a unified
      graphical user interface to allow the user to inspect the peptide
      identifications and also control the way these identifications are used to
      infer the protein identifications.

    </para>

  </listitem>

  <listitem>

    <para>

      Export the data after the results exploration above in a variety of
      formats.

    </para>

  </listitem>

  <listitem>

    <para>

      Perform quantitative proteomics on the basis of the results obtained at
      the previous steps.

    </para>

  </listitem>

  <listitem>

    <para>

      Perform bio-statistical analyses on the quantitative proteomics data
      obtained at the previous step.

    </para>

  </listitem>

</itemizedlist>

  </sect1>

  <sect1 xml:id="sect_preface-feedback-from-users">

    <title>Feedback from the users</title>

    <para>We are always grateful to any constructive feedback from the users.</para>
    <para>The PAPPSO software team might be contacted
    <foreignphrase>via</foreignphrase> the following contact page:</para>

  <para><link
  xlink:href="http://pappso.inrae.fr/en/travailler_avec_nous/contact/"/> (search
  for team members having the <quote>Bioinformatics</quote> specialty
  mentioned, like Olivier Langella or Filippo Rusconi).</para>

</sect1>

<sect1>

  <title>Program and Documentation Availability and License</title>

  <para>The programs and all the documentation that are shipped along with the
  &i2mcq; software suite are available at <link
  xlink:href="http://pappso.inrae.fr/en/bioinfo/xtandempipeline/"/>. Most of the
  time, a new version is published as source, and as binary install packages for
  <productname>MS-Windows</productname> (64-bit systems only).

  </para>

  <para>

    For <productname>GNU/Linux</productname>, binary packages are created
    locally (see <link
    xlink:href="http://pappso.inrae.fr/en/bioinfo/xtandempipeline/download/"/>)
    but are also built in the
    <productname>Debian</productname><footnote><para><link
    xlink:href="http://www.debian.org/"/></para></footnote> autobuilders and are
    uploaded to the distribution servers. These packages are available using the
    system's software management infrastructure (like using the
    <productname>Debian</productname>'s <command>apt</command> command, for
    example, or the graphical application).

  </para>

<para>The software and all the documentation are all provided under the Free
Software license <emphasis>GNU General Public License, Version&nbsp;3, or later,
at your option</emphasis>.  For an in-depth study of the <emphasis>Free
Software</emphasis> philosphy, the reader is kindly urged to visit <link
xlink:href="http://www.gnu.org/philosophy"/>.</para>

  </sect1>

</preface>
