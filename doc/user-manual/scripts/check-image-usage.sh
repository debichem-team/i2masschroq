#!/bin/zsh

pngDir="../images/src/png"

for file in ${pngDir}/*.png
do
  baseName=$(basename ${file})
  printf "Checking ${baseName}... "

  grep -q ${baseName} ../xml/*.xml

  if [ $? -ne 0 ]
  then 
    printf "Not used.\n" 
    mv -v ${file} ${pngDir}/unused
  else 
    printf "Used.\n"
  fi
done
