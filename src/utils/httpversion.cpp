/**
 * \file /utils/httpversion.cpp
 * \date 2/11/2018
 * \author Olivier Langella
 * \brief check for new version on PAPPSO web site
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "httpversion.h"
#include "../config.h"
#include <QDebug>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>

HttpVersion::HttpVersion()
{
  //// http://pappso.inra.fr/demande/ws/pappso_software/i2masschroq
  QSettings settings;
  bool check_online_version =
    settings.value("global/check_online_version", QString("%1").arg(true))
      .toBool();

  if(check_online_version)
    {
      mpa_manager = new QNetworkAccessManager();
      QObject::connect(
        mpa_manager,
        &QNetworkAccessManager::finished,
        this,
        [=](QNetworkReply *reply) {
          if(reply->error())
            {
              qDebug() << " " << reply->errorString();
              return;
            }

          QString answer = reply->readAll();

          qDebug() << answer;

          QJsonDocument d   = QJsonDocument::fromJson(answer.toUtf8());
          QJsonObject sett2 = d.object();
          m_onlineVersion   = sett2.value(QString("version")).toString();
          m_permalink       = sett2.value(QString("permalink")).toString();
          qDebug() << m_onlineVersion;
          qDebug() << m_permalink;
          emit httpVersionReady();
        });

      m_request.setUrl(QUrl(
        QString(
          "http://pappso.inrae.fr/bioinfo/i2masschroq/index.json?from=%1")
          .arg(i2MassChroQ_VERSION)));

      m_request.setHeader(QNetworkRequest::ContentTypeHeader,
                          "application/json");
      m_request.setRawHeader("User-Agent",
                             QString("%1 %2 (%3)")
                               .arg(SOFTWARE_NAME)
                               .arg(i2MassChroQ_VERSION)
                               .arg(BUILD_SYSTEM_NAME)
                               .toLatin1());
      mpa_manager->get(m_request);
    }
}

HttpVersion::~HttpVersion()
{
  delete mpa_manager;
}


bool
HttpVersion::isNewVersion() const
{
  QString current_version(QStringLiteral(i2MassChroQ_VERSION));
  QStringList new_version = m_onlineVersion.split(".");
  int i                   = 0;
  for(QString number : current_version.split("."))
    {
      if(i < new_version.size())
        {
          if(new_version[i].toInt() > number.toInt())
            {
              return true;
            }
          if(new_version[i].toInt() < number.toInt())
            {
              return false;
            }
        }

      i++;
    }
  return false;
}
const QString &
HttpVersion::getVersion() const
{
  return m_onlineVersion;
}

const QString &
HttpVersion::getPermalink() const
{
  return m_permalink;
}
