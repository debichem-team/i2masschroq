/**
 * \file utils/groupstore.cpp
 * \date 28/3/2017
 * \author Olivier Langella
 * \brief store unique version of groups
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "groupstore.h"
#include <pappsomspp/exception/exceptionnotfound.h>

GroupStore::GroupStore()
{
}

GroupStore::~GroupStore()
{
}

void
GroupStore::clear()
{
  _map_group.clear();
}


std::size_t
GroupStore::countGroup() const
{
  return _map_group.size();
}
std::size_t
GroupStore::countSubGroup() const
{

  unsigned int count = 0;
  for(auto &&pair_group : _map_group)
    {
      count += pair_group.second->getNumberOfSubgroups();
    }
  return count;
}

const std::map<unsigned int, GroupingGroupSp> &
GroupStore::getGroupMap() const
{
  return _map_group;
}

GroupingGroupSp
GroupStore::getInstance(unsigned int group_number)
{

  GroupingGroupSp sp_group;
  if(group_number > 0)
    {
      std::map<unsigned int, GroupingGroupSp>::iterator it =
        _map_group.find(group_number);
      if(it != _map_group.end())
        {
          sp_group = it->second;
        }
      else
        {
          sp_group = std::make_shared<GroupingGroup>();
          _map_group.insert(
            std::pair<unsigned int, GroupingGroupSp>(group_number, sp_group));
        }
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("Error in %1 %2 %3 :\n group_number==0")
          .arg(__FILE__)
          .arg(__FUNCTION__)
          .arg(__LINE__));
    }
  return (sp_group);
}
