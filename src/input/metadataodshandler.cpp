/**
 * \file input/metadataodshandler.cpp
 * \date 13/01/2021
 * \author Thomas Renne
 * \brief set MCQR params and run
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "metadataodshandler.h"
#include <QDebug>
#include <pappsomspp/exception/exceptionoutofrange.h>

MetadataOdsHandler::MetadataOdsHandler(ProjectSp project)
{
  m_rowNumber   = 0;
  m_sheetNumber = 0;
  msp_project   = project;
}

MetadataOdsHandler::~MetadataOdsHandler()
{
}

void
MetadataOdsHandler::startSheet(const QString &sheet_name)
{
  qDebug() << sheet_name << " " << m_sheetNumber;
  if(m_sheetNumber != 0)
    {
      throw pappso::ExceptionOutOfRange(QObject::tr(
        "ERROR this file is not a metada ODS file : only one data sheet is "
        "allowed to describe metadata"));
    }
  msp_project->getMcqrExperimentSp()
    .get()
    ->clearMetadataInfoAndColumnsVectors();
  m_sheetNumber++;
}

void
MetadataOdsHandler::startLine()
{
  qDebug();
  m_colNumber                    = 0;
  m_mcqrMetadataLine.m_msrunFile = "";
  m_mcqrMetadataLine.m_msrunId   = "";
  m_mcqrMetadataLine.m_otherData.clear();
}

void
MetadataOdsHandler::setCell(const OdsCell &cell)
{
  qDebug();
  if(!cell.toString().isEmpty())
    {
      qDebug();
      if(m_rowNumber == 0)
        {
          qDebug() << cell.toString();
          msp_project->getMcqrExperimentSp().get()->addColumnName(
            cell.toString());
          m_columnNames.push_back(cell.toString());
        }
      else
        {
          qDebug();
          if(m_colNumber == 0)
            {
              qDebug();
              m_mcqrMetadataLine.m_msrunId = cell.toString().simplified();
            }
          else if(m_colNumber == 1)
            {
              qDebug();
              m_mcqrMetadataLine.m_msrunFile = cell.toString();
            }
          else
            {
              if(!m_mcqrMetadataLine.m_msrunId.isEmpty())
                {
                  qDebug() << cell.toString();
                  QString column_name;
                  if(m_colNumber < m_columnNames.size())
                    {
                      column_name = m_columnNames.at(m_colNumber);
                      if(cell.isDouble())
                        {
                          m_mcqrMetadataLine.m_otherData.insert(
                            std::pair<QString, QVariant>(
                              column_name, cell.getDoubleValue()));
                        }
                      else if(cell.isString())
                        {
                          m_mcqrMetadataLine.m_otherData.insert(
                            std::pair<QString, QVariant>(
                              column_name, cell.getStringValue()));
                        }
                      else if(cell.isBoolean())
                        {
                          m_mcqrMetadataLine.m_otherData.insert(
                            std::pair<QString, QVariant>(
                              column_name, cell.getBooleanValue()));
                        }
                      else
                        {
                          m_mcqrMetadataLine.m_otherData.insert(
                            std::pair<QString, QVariant>(column_name,
                                                         cell.toString()));
                        }
                    }
                  else
                    {
                      qDebug() << "problem with cell " << cell.toString();

                      throw pappso::PappsoException(
                        QObject::tr(
                          "ERROR the cell %1 at line %2 has no column name")
                          .arg(cell.toString())
                          .arg(this->m_rowNumber));
                    }
                }
              qDebug();
            }
        }
      m_colNumber++;
    }
  qDebug();
}

void
MetadataOdsHandler::endLine()
{
  qDebug();
  if(m_rowNumber != 0)
    {
      try
        {
          if(!m_mcqrMetadataLine.m_msrunId.isEmpty())
            {
              msp_project->getMcqrExperimentSp().get()->addNewMcqrMetadataLine(
                m_mcqrMetadataLine);
            }
        }
      catch(pappso::PappsoException &error)
        {
          throw pappso::PappsoException(
            QObject::tr("error reading metadata file at line %1\n%2")
              .arg(m_rowNumber + 1)
              .arg(error.qwhat()));
        }
    }
  m_rowNumber++;
}

void
MetadataOdsHandler::endSheet()
{
}

void
MetadataOdsHandler::endDocument()
{
}
