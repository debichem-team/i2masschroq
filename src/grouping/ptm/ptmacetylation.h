/**
 * \file grouping/ptm/ptmacetylation.h
 * \date 16/06/2020
 * \author Olivier Langella
 * \brief PTM methylation
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "ptminterface.h"

/**
 * @todo write docs
 */
class PtmAcetylation : public PtmInterface
{
  public:
  /**
   * Default constructor
   */
  PtmAcetylation();

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  PtmAcetylation(const PtmAcetylation &other);

  /**
   * Destructor
   */
  ~PtmAcetylation();


  virtual std::vector<unsigned int>
  getPtmPositions(const PeptideMatch &peptide_match) const override;

  private:
  pappso::AaModificationP mp_acetylation; // MOD:00394
};
