/**
 * \file grouping/ptm/ptminterface.h
 * \date 15/06/2020
 * \author Olivier Langella
 * \brief PTM interface
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <vector>
#include <pappsomspp/amino_acid/aamodification.h>

#include "../../core/peptidematch.h"
#include "../../core/proteinmatch.h"


/**
 * @brief Interface to describe a post translational modification
 */
class PtmInterface
{
  public:
  /**
   * Default constructor
   */
  PtmInterface();

  /**
   * Destructor
   */
  virtual ~PtmInterface();


  virtual std::size_t
  countPeptideMatchPtm(const PeptideMatch &peptide_match) const final;

  virtual std::vector<unsigned int>
  getPtmPositions(const PeptideMatch &peptide_match) const = 0;


  virtual std::vector<unsigned int>
  getPtmPositions(const ProteinMatch *protein_match) const final;
};
