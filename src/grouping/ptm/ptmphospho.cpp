/**
 * \file grouping/ptm/ptmphospho.cpp
 * \date 15/06/2020
 * \author Olivier Langella
 * \brief PTM phosphorylation
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "ptmphospho.h"

PtmPhospho::PtmPhospho()
{

  // phospho :
  m_modificationList.push_back(
    {pappso::AaModification::getInstance("MOD:00696"), {}});
  m_modificationList.push_back(
    {pappso::AaModification::getInstance("MOD:00704"), {'S', 'T'}});
}

PtmPhospho::~PtmPhospho()
{
}

std::vector<unsigned int>
PtmPhospho::getPtmPositions(const PeptideMatch &peptide_match) const
{
  std::vector<unsigned int> position_list;

  for(const ModificationAndAa &modification : m_modificationList)
    {
      if(modification.aa_list.size() == 0)
        {
          for(unsigned int position :
              peptide_match.getPeptideEvidence()
                ->getPeptideXtpSp()
                .get()
                ->getModificationPositionList(modification.modification))
            {
              position_list.push_back(position);
            }
        }
      else
        {
          for(unsigned int position :
              peptide_match.getPeptideEvidence()
                ->getPeptideXtpSp()
                .get()
                ->getModificationPositionList(modification.modification,
                                              modification.aa_list))
            {
              position_list.push_back(position);
            }
        }
    }

  std::sort(position_list.begin(), position_list.end());
  auto last = std::unique(position_list.begin(), position_list.end());
  position_list.erase(last, position_list.end());
  return position_list;
}
