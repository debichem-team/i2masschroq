/**
 * \file grouping/ptm/ptmislandsubgroup.h
 * \date 29/5/2017
 * \author Olivier Langella
 * \brief object to group ptm islands sharing the same set of sample scans with
 * different proteins
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMISLANDSUBGROUP_H
#define PTMISLANDSUBGROUP_H
#include <memory>
#include "ptmisland.h"


class PtmIslandSubgroup;

/** \brief shared pointer on a Peptide object
 */
typedef std::shared_ptr<PtmIslandSubgroup> PtmIslandSubgroupSp;

class PtmIslandGroup;

class PtmIslandSubgroup
{
  public:
  PtmIslandSubgroup(PtmIslandSp ptm_island_sp);
  PtmIslandSubgroup(const PtmIslandSubgroup &other);
  ~PtmIslandSubgroup();

  /** @brief merge with the given ptm island if all sample scans are shared
   * */
  bool mergePtmIslandSp(PtmIslandSp ptm_island_sp);
  bool containsProteinMatchFrom(PtmIslandSubgroup *ptm_island_subgroup_p);

  std::size_t countSampleScan() const;
  const std::vector<PtmIslandSp> &getPtmIslandList() const;

  void setPtmIslandGroup(PtmIslandGroup *p_ptm_island_group);

  const PtmIsland *getFirstPtmIsland() const;
  unsigned int getSubgroupNumber() const;
  void setSubgroupNumber(unsigned int number);
  const PtmIslandGroup *getPtmIslandGroup() const;

  private:
  std::vector<PtmIslandSp> _ptm_island_list;
  std::vector<std::size_t> _sample_scan_set;
  PtmIslandGroup *_ptm_island_group_p = nullptr;
  unsigned int _subgroup_number;
};

#endif // PTMISLANDSUBGROUP_H
