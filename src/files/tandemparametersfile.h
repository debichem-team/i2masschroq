/**
 * \file /files/tandemparametersfile.h
 * \date 19/9/2017
 * \author Olivier Langella
 * \brief handles X!Tandem parameters file (presets)
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QFileInfo>
#include <QXmlStreamWriter>
#include "../core/tandem_run/tandemparameters.h"
#include "../input/tandem/tandemparamparser.h"

class TandemParametersFile
{

  public:
  TandemParametersFile(const QString &param_file);
  TandemParametersFile(const QFileInfo &param_file);
  TandemParametersFile(const TandemParametersFile &other);
  ~TandemParametersFile();

  /** @brief return the preset file name
   * File name if its a TandemPresetFile
   * or "list path, default parameters" value if its a Tandem result
   */
  const QString &getMethodName() const;
  const QString getFilename() const;
  const QDir getAbsoluteDir() const;
  const QString getAbsoluteFilePath() const;
  bool exists() const;

  /** @brief tells if this file is really a tandem preset file
   */
  bool isTandemPresetFile();

  /** @brief read tandem parameters from XML file
   */
  TandemParameters getTandemParameters();

  /** @brief write tandem parameters  to XML file
   */
  void setTandemParameters(const TandemParameters &parameters);

  /** @brief set param file directory
   */
  void setDirectory(const QDir &directory);

  private:
  void writeXmlParametersFile(QXmlStreamWriter *p_out,
                              const TandemParameters &parameters) const;
  void setTandemParametersFileType(const TandemParamParser &param_parser);

  private:
  QFileInfo _param_source;
  bool m_isTandemParameterFile = false;
  ParamFileType m_fileType     = ParamFileType::empty;
  QString m_methodName         = "";
};
