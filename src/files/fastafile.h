
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QFileInfo>
#include <memory>
#include "../utils/proteinstore.h"

class FastaFile;
typedef std::shared_ptr<FastaFile> FastaFileSp;

class FastaFile
{
  public:
  FastaFile(const QString &fasta_source);
  FastaFile(const QFileInfo &fasta_source);
  FastaFile(const FastaFile &other);
  ~FastaFile();


  const QString getFilename() const;
  const QString getAbsoluteFilePath() const;

  /** @brief read fasta file and set accessions as contaminants
   * */
  void setContaminants(ProteinStore &protein_store) const;

  /** @brief read fasta file and set accessions as decoys
   * */
  void setDecoys(ProteinStore &protein_store) const;

  /** @brief sets the fasta filename
   */
  void setFastaSource(const QString &fasta_file_location);


  void setXmlId(const QString xmlid);
  const QString &getXmlId() const;

  private:
  QFileInfo m_fastaSource;
  QString m_xmlId;
};

Q_DECLARE_METATYPE(FastaFile *)
Q_DECLARE_METATYPE(FastaFileSp)
