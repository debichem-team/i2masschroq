/**
 * \file /output/ods/samplesheet.h
 * \date 8/5/2017
 * \author Olivier Langella
 * \brief ODS sample sheet
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef SAMPLESHEET_H
#define SAMPLESHEET_H

#include "../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "odsexport.h"

class SampleSheet
{
  public:
  SampleSheet(OdsExport *p_ods_export,
              CalcWriterInterface *p_writer,
              const Project *p_project);

  private:
  void writeHeaders();
  void
  writeIdentificationDataSource(IdentificationDataSource *p_ident_data_source);

  private:
  OdsExport *_p_ods_export;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
  bool _msrun_statistics = true;
};

#endif // SAMPLESHEET_H
