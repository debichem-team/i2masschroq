/**
 * \file /output/ods/ptm/ptmspectrasheet.h
 * \date 16/2/2018
 * \author Olivier Langella
 * \brief ODS PTM spectra sheet
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMSPECTRASHEET_H
#define PTMSPECTRASHEET_H

#include "../../../core/project.h"
#include <odsstream/calcwriterinterface.h>
#include "../../../grouping/ptm/ptmisland.h"
#include "../odsexport.h"
#include "../spectrasheet.h"

class PtmSpectraSheet
{
  public:
  PtmSpectraSheet(OdsExport *p_ods_export,
                  CalcWriterInterface *p_writer,
                  const Project *p_project);

  protected:
  PtmSpectraSheet(OdsExport *p_ods_export,
                  CalcWriterInterface *p_writer,
                  const Project *p_project,
                  const QString &sheet_name);
  virtual void writeIdentificationGroup(IdentificationGroup *p_ident);
  void writeHeaders(IdentificationGroup *p_ident);
  void writeBestPeptideEvidence(
    const PtmSampleScan &ptm_sample_scan,
    std::set<const PtmIsland *> &ptmisland_occurence_list);
  void writeCellHeader(PeptideListColumn column);

  void
  writePtmIslandSubgroupSp(const PtmIslandSubgroupSp &ptm_island_subgroup_sp);

  protected:
  OdsExport *_p_ods_export;
  const PtmGroupingExperiment *_p_ptm_grouping_experiment;
  const Project *_p_project;
  CalcWriterInterface *_p_writer;
};

#endif // PTMSPECTRASHEET_H
