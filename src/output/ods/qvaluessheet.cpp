/**
 * \file /output/ods/qvaluessheet.cpp
 * \date 16/09/2019
 * \author Olivier Langella
 * \brief ODS Q-values sheet
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "qvaluessheet.h"
#include "../../core/qvalue/computeqvalues.h"

QvaluesSheet::QvaluesSheet(OdsExport *p_ods_export,
                           CalcWriterInterface *p_writer,
                           ProjectSp p_project)
{
  _p_writer     = p_writer;
  _p_ods_export = p_ods_export;
  p_writer->writeSheet("q-values");

  OdsTableSettings table_settings;
  table_settings.setVerticalSplit(1);
  _p_writer->setCurrentOdsTableSettings(table_settings);

  // writeHeaders();

  ComputeQvalues qvalues(p_project.get());

  qvalues.writeDistributionsByEngines(_p_writer);
}
