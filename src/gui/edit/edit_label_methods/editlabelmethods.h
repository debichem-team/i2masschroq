
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QDialog>
#include <QStandardItemModel>
#include "../../../core/project.h"
#include "../../../core/labeling/labelingmethod.h"

class ProjectWindow;

namespace Ui
{
class EditLabelMethodView;
}

class EditLabelMethods : public QDialog
{
  Q_OBJECT
  public:
  explicit EditLabelMethods(ProjectWindow *parent = 0);
  ~EditLabelMethods();

  void setProjectSp(ProjectSp project_sp);
  LabelingMethodSp getLabelingMethodSp() const;

  public slots:
  void ItemClicked(QModelIndex index);

  private:
  Ui::EditLabelMethodView *ui;
  ProjectWindow *_project_window;
  ProjectSp _project_sp;
  QStandardItemModel *_p_label_method_str_li = nullptr;
  LabelingMethodSp _sp_labeling_method;
};
