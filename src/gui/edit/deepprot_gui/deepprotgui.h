/**
 * \file src/gui/edit/deepprot_gui/deepprotgui.h
 * \date 27/1/2021
 * \author Olivier Langella
 * \brief GUI dedicated to DeepProt postprocessing
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QStandardItemModel>
#include "../../../core/project.h"
#include "../../widgets/qcp_massdelta/massdeltawidget.h"
#include "modification_browser/modificationbrowser.h"


class ProjectWindow;

namespace Ui
{
class DeepProtGui;
}

class DeepProtGui : public QMainWindow
{
  Q_OBJECT
  public:
  explicit DeepProtGui(ProjectWindow *parent, ProjectSp project_sp);

  /**
   * Destructor
   */
  ~DeepProtGui();

  void closeEvent(QCloseEvent *event) override;

  public slots:
  // void peptideEdited(QString peptideStr);
  // void setColor(const QColor &color);
  // void setShape(Shape shape);
  void doFilterChangeFinished();
  void doProjectNameChanged(QString name);
  void doPeptideEvidenceChanged(
    std::shared_ptr<PeptideEvidence> peptide_evidence_sp);
  void doSelectedPeptideEvidenceChanged(
    std::shared_ptr<PeptideEvidence> peptide_evidence_sp);

  private:
  void setPeptideEvidenceList();
  std::vector<std::shared_ptr<PeptideEvidence>>
  generatePeptideEvidenceSolutions(
    std::shared_ptr<PeptideEvidence> &peptide_evidence_sp) const;

  void addPushButtonAlternativeSequences();

  private slots:
  void doClickPeptideDetailView();
  void showModificationsBrowser();

  private:
  Ui::DeepProtGui *ui;
  ProjectWindow *mp_projectWindow;
  ProjectSp msp_project;
  std::shared_ptr<PeptideEvidence> msp_selectedPeptideEvidence = nullptr;
  std::vector<std::shared_ptr<PeptideEvidence>>
    m_alternativePeptideEvidenceList;

  ModificationBrowser *mpa_modificationBrowser = nullptr;
};
