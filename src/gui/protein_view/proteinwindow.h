
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PROTEINWINDOW_H
#define PROTEINWINDOW_H

#include <QMainWindow>
#include <QTextDocument>
#include <QPushButton>
#include "../../core/proteinmatch.h"

class DbXrefButton : public QPushButton
{
  Q_OBJECT
  public:
  DbXrefButton(QWidget *parent, DbXref dbxref);
  protected slots:
  void clickedSlot();

  private:
  DbXref _dbxref;
};

class ProjectWindow;

namespace Ui
{
class ProteinDetailView;
}

class ProteinWindow : public QMainWindow
{
  Q_OBJECT

  public:
  explicit ProteinWindow(ProjectWindow *parent = 0);
  ~ProteinWindow();
  void setProteinMatch(ProteinMatch *p_protein_match);

  public slots:
  void
  doIdentificationGroupGrouped(IdentificationGroup *p_identification_group);
  void doPeptideEvidenceSelected(PeptideEvidence *peptide_evidence);
  void doProjectNameChanged(QString name);
  protected slots:
  void browseUrl(int i);
  void doSaveSvg();

  protected:
  void updateDisplay();
  void clearDbXrefLayout();

  private:
  Ui::ProteinDetailView *ui;
  QTextDocument sequence_text;
  ProjectWindow *_p_project_window;
  ProteinMatch *_p_protein_match = nullptr;
};

#endif // PROTEINWINDOW_H
