
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "choosemodificationdialog.h"

#include "ui_choose_modification_dialog.h"
#include <QDebug>
#include <QDesktopServices>
#include "../../utils/utils.h"
#include <pappsomspp/amino_acid/aamodification.h>

ChooseModificationDialog::ChooseModificationDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::ChooseModificationDialog)
{
  qDebug();
  ui->setupUi(this);
  this->setModal(true);

  // param.setFilterCrossSamplePeptideNumber(settings.value("automatic_filter/cross_sample",
  // "true").toBool());
}

ChooseModificationDialog::~ChooseModificationDialog()
{
  qDebug();
  delete ui;

  qDebug();
}


pappso::AaModificationP
ChooseModificationDialog::getSelectedModification() const
{
  if(ui->oboChooserWidget->isOboTermSelected())
    {
      return pappso::AaModification::getInstance(
        ui->oboChooserWidget->getOboPsiModTermSelected());
    }
  return nullptr;
}

void
ChooseModificationDialog::setMzTarget(double target_mz)
{
  ui->oboChooserWidget->setMzTarget(target_mz);
}

void
ChooseModificationDialog::setPrecision(pappso::PrecisionPtr precision)
{
  ui->oboChooserWidget->setPrecision(precision);
}
