/**
 * \file gui/masschroq_run_dialog/masschroqrundialog.h
 * \date 25/01/2019
 * \author Olivier Langella
 * \brief choose ODS export options
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialog>
#include <QStandardItemModel>

#include "../../core/project.h"
#include "../../core/masschroq_run/masschroqrunbatch.h"
#include "../workerthread.h"


namespace Ui
{
class MassChroqRunDialog;
}

class MainWindow;

class MassChroqRunDialog : public QDialog
{
  Q_OBJECT


  public:
  explicit MassChroqRunDialog(MainWindow *parent, WorkerThread *p_worker);
  ~MassChroqRunDialog();
  void accept() override;
  void reject() override;

  void setMasschroqFileParameters();

  void setProjectSPtr(ProjectSp p_project);
  void setAlignmentGroups();
  bool getMassChroqRunStatus();
  MassChroQRunBatch getMassChroqRunBatchParam();
  void setMassChroqRunBatchParam(QString masschroqml_file);
  void updateAlignmentGroupsStatus(QString masschroqml_file);


  void updateMasschroqFileParameters() const;

  signals:
  // void peptideChanged(pappso::PeptideSp peptide);
  void operateFindBestMsrunForAlignment(ProjectSp project_sp,
                                        MsRunAlignmentGroupSp alignment_group);
  void operateCheckingMsrunFilePath(ProjectSp project_sp);
  void operateAcceptedMassChroqRunDialog();


  public slots:
  void setBestMsrunForAlignment(MsRunSp msrun_sp);
  void setCheckMsrunFilePathOk(MsRunSp msrun_sp);

  protected slots:
  void doCheckMsrunFilepath();
  void doBrowseMsrunDirectory();
  void doFindBestMsrunForAlignment();
  void doShowMsRunsInAlignmentGroup(QModelIndex index);
  void doUpdateReferenceInSelectedGroup(int index);
  void doRunMassChroQ();
  void doSaveMassChroQML();
  void doBrowseMassChroQBin();
  void doBrowseTempDir();
  void doUpdateComparButton();
  void checkTabToEnableMassChroQRun(int tab_position);

  private:
  void setMassChroQRunParamTab();
  void setComparFileValue(bool write_compar);

  private:
  Ui::MassChroqRunDialog *ui;
  ProjectSp msp_project;
  MainWindow *mp_main;
  bool m_writeComparFile   = false;
  bool m_runMassChroqAsked = false;
  MassChroQRunBatch m_masschroqBatchParam;
  std::vector<MsRunAlignmentGroupSp> msp_alignmentGroups;
  MsRunAlignmentGroupSp msp_allMsrunAlignmentGroup;
  QStandardItemModel *mp_poModel;
  MsRunAlignmentGroupSp msp_selected_group;
  MasschroqFileParametersSp msp_masschroqFileParametersSp;
};
