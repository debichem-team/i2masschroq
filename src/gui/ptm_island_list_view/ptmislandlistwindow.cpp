/**
 * \file gui/ptm_island_list_window/ptmislandlistwindow.cpp
 * \date 30/5/2017
 * \author Olivier Langella
 * \brief display all ptm islands
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptmislandlistwindow.h"


#include "ui_ptm_island_list_view.h"
#include "../mainwindow.h"
#include <QSettings>
#include "../ptm_peptide_list_view/ptmpeptidelistwindow.h"

PtmIslandListQActionColumn::PtmIslandListQActionColumn(
  PtmIslandListWindow *parent, PtmIslandListColumn column)
  : QAction(parent)
{
  this->setText(PtmIslandTableModel::getTitle(column));

  this->setCheckable(true);
  this->setChecked(parent->getPtmIslandListColumnDisplay(column));

  m_column           = column;
  mp_ptmIslandWindow = parent;

  connect(this,
          &PtmIslandListQActionColumn::toggled,
          this,
          &PtmIslandListQActionColumn::doToggled);
}

PtmIslandListQActionColumn::~PtmIslandListQActionColumn()
{
}

void
PtmIslandListQActionColumn::doToggled(bool toggled)
{
  setChecked(toggled);
  mp_ptmIslandWindow->setPtmIslandListColumnDisplay(m_column, toggled);
}


PtmIslandListWindow::PtmIslandListWindow(ProjectWindow *parent)
  : QMainWindow(parent), ui(new Ui::PtmIslandListWindow)
{
  _project_window = parent;
  ui->setupUi(this);
  /*
   */
  _ptm_table_model_p = new PtmIslandTableModel(this);


  _ptm_proxy_model_p = new PtmIslandProxyModel(this, _ptm_table_model_p);
  _ptm_proxy_model_p->setSourceModel(_ptm_table_model_p);
  _ptm_proxy_model_p->setDynamicSortFilter(true);
  ui->ptm_island_tableview->setModel(_ptm_proxy_model_p);
  ui->ptm_island_tableview->setSortingEnabled(true);
  ui->ptm_island_tableview->setAlternatingRowColors(true);
  ui->ptm_island_tableview->horizontalHeader()->setSectionsMovable(true);

  _statusbar_ptm_islands_label = new QLabel("");
  ui->statusbar->addWidget(_statusbar_ptm_islands_label);

  _statusbar_displayed_label = new QLabel("");
  ui->statusbar->addWidget(_statusbar_displayed_label);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(this,
          &PtmIslandListWindow::ptmIslandDataChanged,
          _ptm_table_model_p,
          &PtmIslandTableModel::onPtmIslandDataChanged);

  connect(_project_window,
          &ProjectWindow::identificationPtmGroupGrouped,
          this,
          &PtmIslandListWindow::doIdentificationPtmGroupGrouped);
  connect(_project_window,
          &ProjectWindow::identificationGroupGrouped,
          this,
          &PtmIslandListWindow::doIdentificationGroupGrouped);
  connect(ui->ptm_island_tableview,
          &QTableView::clicked,
          _ptm_proxy_model_p,
          &PtmIslandProxyModel::onTableClicked);
  connect(_ptm_table_model_p,
          &PtmIslandTableModel::layoutChanged,
          this,
          &PtmIslandListWindow::updateStatusBar);
#else
  // Qt4 code
  connect(this,
          SIGNAL(ptmIslandDataChanged()),
          _ptm_table_model_p,
          SLOT(onPtmIslandDataChanged()));

  connect(_project_window,
          SIGNAL(identificationPtmGroupGrouped(IdentificationGroup *)),
          this,
          SLOT(doIdentificationPtmGroupGrouped(IdentificationGroup *)));
  connect(_project_window,
          SIGNAL(identificationGroupGrouped(IdentificationGroup *)),
          this,
          SLOT(doIdentificationGroupGrouped(IdentificationGroup *)));
  connect(ui->ptm_island_tableview,
          SIGNAL(clicked(const QModelIndex &)),
          _ptm_proxy_model_p,
          SLOT(onTableClicked(const QModelIndex &)));

#endif
}

PtmIslandListWindow::~PtmIslandListWindow()
{
  delete _ptm_table_model_p;
  delete _ptm_proxy_model_p;
  delete _statusbar_displayed_label;
}

ProjectWindow *
PtmIslandListWindow::getProjectWindowP()
{
  return _project_window;
}
void
PtmIslandListWindow::setIdentificationGroup(
  IdentificationGroup *p_identification_group)
{

  qDebug() << "PtmIslandListWindow::setIdentificationGroup begin ";
  if(p_identification_group != nullptr)
    {
      qDebug() << "PtmIslandListWindow::setIdentificationGroup not null";
      _p_identification_group = p_identification_group;
      _ptm_table_model_p->setIdentificationGroup(p_identification_group);
      //_p_proxy_model->setSourceModel(_protein_table_model_p);
    }
  else
    {
      qDebug() << "PtmIslandListWindow::setIdentificationGroup  null";
    }

  qDebug() << "PtmIslandListWindow::setIdentificationGroup end";
}

void
PtmIslandListWindow::setPtmIslandListColumnDisplay(PtmIslandListColumn column,
                                                   bool toggled)
{
  _ptm_proxy_model_p->setPtmIslandListColumnDisplay(column, toggled);
}

bool
PtmIslandListWindow::getPtmIslandListColumnDisplay(
  PtmIslandListColumn column) const
{
  return _ptm_proxy_model_p->getPtmIslandListColumnDisplay(column);
}

void
PtmIslandListWindow::setColumnMenuList()
{
  if(ui->menu_Columns->isEmpty())
    {
      PtmIslandListQActionColumn *p_action;
      for(int i = 0; i < _ptm_table_model_p->columnCount(); i++)
        {
          p_action = new PtmIslandListQActionColumn(
            this, PtmIslandTableModel::getPtmIslandListColumn(i));
          ui->menu_Columns->addAction(p_action);
        }
    }
}

void
PtmIslandListWindow::edited()
{
  _project_window->doPtmIslandGrouping(_p_identification_group);
}

void
PtmIslandListWindow::doNotCheckedHide(bool hide)
{
  _ptm_proxy_model_p->hideNotChecked(hide);
  emit ptmIslandDataChanged();
}


void
PtmIslandListWindow::doIdentificationGroupGrouped(
  IdentificationGroup *p_identification_group)
{
  qDebug() << "PtmIslandListWindow::doIdentificationGroupGrouped begin";
  if(_p_identification_group == p_identification_group)
    {
      // ui->ptm_island_tableview->resizeRowToContents(_ptm_table_model_p->rowCount());
      _project_window->doPtmIslandGrouping(p_identification_group);
      // emit ptmIslandDataChanged();
    }

  qDebug() << "PtmIslandListWindow::doIdentificationGroupGrouped end";
}

void
PtmIslandListWindow::doIdentificationPtmGroupGrouped(
  IdentificationGroup *p_identification_group)
{
  qDebug() << "PtmIslandListWindow::doIdentificationPtmGroupGrouped begin";
  if(_p_identification_group == p_identification_group)
    {
      // ui->ptm_island_tableview->resizeRowToContents(0);
      //_protein_table_model_p->setIdentificationGroup(p_identification_group);
      //_p_proxy_model->setSourceModel(_protein_table_model_p);
      _ptm_table_model_p->setIdentificationGroup(p_identification_group);
      emit ptmIslandDataChanged();

      const PtmGroupingExperiment *p_ptm_grouping_experiment =
        p_identification_group->getPtmGroupingExperiment();
      _statusbar_ptm_islands_label->setText(
        tr("ptm island groups:%1 ptm island subgroups:%2 ptm islands:%3")
          .arg(p_ptm_grouping_experiment->getPtmIslandGroupList().size())
          .arg(p_ptm_grouping_experiment->getPtmIslandSubgroupList().size())
          .arg(p_ptm_grouping_experiment->getPtmIslandList().size()));
      // ui->ptm_island_tableview->resizeRowToContents(_ptm_table_model_p->rowCount());
    }
  setColumnMenuList();
  qDebug() << "PtmIslandListWindow::doIdentificationPtmGroupGrouped end";
}

void
PtmIslandListWindow::resizeColumnsToContents()
{
  ui->ptm_island_tableview->resizeColumnsToContents();
}

void
PtmIslandListWindow::connectNewPtmPeptideListWindow()
{
  qDebug() << "PtmIslandListWindow::connectNewPtmPeptideListWindow begin";
  _p_current_ptm_peptide_list_window = new PtmPeptideListWindow(this);
  _ptm_peptide_list_window_collection.push_back(
    _p_current_ptm_peptide_list_window);

  qDebug() << "PtmIslandListWindow::connectNewPtmPeptideListWindow end";
}
void
PtmIslandListWindow::askViewPtmPeptideList(PtmIsland *ptm_island)
{
  if(_ptm_peptide_list_window_collection.size() == 0)
    {
      connectNewPtmPeptideListWindow();
    }
  Qt::KeyboardModifiers modifier = QApplication::keyboardModifiers();
  if(modifier == Qt::ControlModifier)
    {
      connectNewPtmPeptideListWindow();
    }


  _p_current_ptm_peptide_list_window->setPtmIsland(ptm_island);
  _p_current_ptm_peptide_list_window->show();
}

void
PtmIslandListWindow::askProteinDetailView(ProteinMatch *p_protein_match)
{
  qDebug() << "begin";
  _project_window->doViewProteinDetail(p_protein_match);
  qDebug() << "end";
  // updateStatusBar();
}

const IdentificationGroup *
PtmIslandListWindow::getIdentificationGroup() const
{
  return _p_identification_group;
}

void
PtmIslandListWindow::updateStatusBar()
{
  if(_p_identification_group == nullptr)
    {
    }
  else
    {
      _statusbar_displayed_label->setText(
        tr("displayed: %1").arg(_ptm_proxy_model_p->rowCount()));
    }
}

void
PtmIslandListWindow::doPtmSearchEdit(QString ptm_search_string)
{
  _ptm_proxy_model_p->setPtmSearchString(ptm_search_string);
  emit ptmIslandDataChanged();
}
void
PtmIslandListWindow::doSearchOn(QString search_on)
{
  _ptm_proxy_model_p->setSearchOn(search_on);
  emit ptmIslandDataChanged();
}
