/**
 * \file gui/mcqr_run_view/mcqr_sds_xic_widget/mcqrsdsxicwidget.h
 * \date 27/07/2021
 * \author Thomas Renne
 * \brief widget to run the XIC part from SDS script from MCQR
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QStandardItemModel>
#include <grantlee/engine.h>
#include "../mcqrparamwidget.h"
#include "../../../core/mcqr/mcqrexperiment.h"

namespace Ui
{
class McqrSdsXicWidget;
}

enum class SdsXicAnalysisStep : std::int8_t
{
  quality_xic            = 0,
  filtering_data         = 1,
  reconstituting_tracks  = 2,
  normalise              = 3,
  filtering_shared       = 4,
  filtering_uncorrelated = 5,
  imputing               = 6,
  heatmap                = 7,
  protein_clustering     = 8,
  anova                  = 9,
  protein_abundance      = 10,
  protein_fc_abundance   = 11,
  last                   = 12,
};

class McqrRunView;
class McqrSdsXicWidget : public McqrParamWidget
{
  Q_OBJECT

  public:
  McqrSdsXicWidget(McqrRunView *mcqr_window,
                   const McqrExperimentSp &p_mcqr_experiment);
  ~McqrSdsXicWidget();
  void changeStepAfterEndTag(QString end_name) override;

  protected slots:
  void changeToolBoxHeight(int tab_selected);
  void runQualityCheckingXic();
  void runFilteringDubiousData();
  void runReconstitutingTracks();
  void runNormaliseIntensities();
  void runFilteringSharedPeptides();
  void runFilteringUncorrelatedPeptides();
  void runImputingPeptidesProteins();
  void runHeatMapProteinAbundance();
  void runProteinClustering();
  void runProteinAnova();
  void runProteinAbundance();
  void runProteinFcAbundance();
  void updateLevelToRemoveList(QString factor);
  void updateNumeratorAndDenominatorComboboxFc(QString factor);

  private:
  void disableFutureStepsInToolBox();
  void initGrantleeEngine();
  void addFactorsToListViews();
  void configureFilteringTab();
  void configureReconstitutingTab();
  void configureNormaliseTab();
  void configureFcFactorList();

  private:
  Ui::McqrSdsXicWidget *ui;
  SdsXicAnalysisStep m_ongoingMcqrStep        = SdsXicAnalysisStep::quality_xic;
  Grantlee::Engine *mp_grantleeEngine         = nullptr;
  Grantlee::Template msp_rscriptTemplate      = nullptr;
  QStandardItemModel *mp_factorsColorModel    = nullptr;
  QStandardItemModel *mp_factorsLabelModel    = nullptr;
  QStandardItemModel *mp_factorsListModel     = nullptr;
  QStandardItemModel *mp_factorLevelsToRemove = nullptr;
  McqrLoadDataMode m_mcqrLoadDataMode;
};
