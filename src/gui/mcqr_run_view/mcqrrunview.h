/**
 * \file gui/mcqr_run_view/mcqrrunview.h
 * \date 30/04/2021
 * \author Thomas Renne
 * \brief widget to run MCQR
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QWidget>
// #include <QProcess>
#include <QTextDocument>
#include <QMenu>
#include <QStandardItemModel>
#include <QMovie>
#include <QCloseEvent>
#include <grantlee/engine.h>
#include "../../core/mcqr/mcqrexperiment.h"
#include "mcqrparamwidget.h"
#include "../../core/mcqr/mcqrqprocess.h"
// #include <gui/mcqr_run_view/mcqr_sds_widget/mcqrsdswidget.h>

namespace Ui
{
class McqrRunView;
}

class MainWindow;
class McqrRunView : public QWidget
{
  Q_OBJECT

  public:
  explicit McqrRunView(MainWindow *parent,
                       const McqrExperimentSp &p_mcqr_experiment);
  ~McqrRunView();
  McqrQProcess *getRProcessRunning();

  protected:
  void closeEvent(QCloseEvent *event) override;

  signals:
  void saveRscriptAndRdata();
  void closeMcqrRunView();

  protected slots:
  void updateMcqrResultsBrowser();
  void handleMcqrMessages();
  void printToPdf();
  void saveRunRscript();
  void saveRData();

  private:
  void checkLibrariesInstalled();
  void loadSpecificMcqrParameterWidget();
  void handleMcqrErrors(QString error_message);

  private:
  Ui::McqrRunView *ui;
  MainWindow *mp_main            = nullptr;
  McqrParamWidget *mp_mcqrParam  = nullptr;
  QMovie *mp_loadingIcon         = nullptr;
  McqrQProcess *mp_rProcess      = nullptr;
  QTextDocument *mp_documentText = nullptr;
  McqrExperimentSp msp_mcqrExperiment;
  QString m_documentString;
};
