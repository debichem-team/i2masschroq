/**
 * \file gui/mcqr_run_view/mcqr_sds_widget/mcqrsdswidget.h
 * \date 30/06/2021
 * \author Thomas Renne
 * \brief widget to run SDS script from MCQR
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QStandardItemModel>
#include <grantlee/engine.h>
#include "../mcqrparamwidget.h"
#include "../../../core/mcqr/mcqrexperiment.h"

namespace Ui
{
class McqrSdsWidget;
}

enum class SdsAnalysisStep : std::int8_t
{
  analyzing_sc         = 0,
  filtering_sc         = 1,
  heatmap_abundance    = 2,
  protein_clustering   = 3,
  protein_anova        = 4,
  protein_abundance    = 5,
  protein_fc_abundance = 6,
  last                 = 7,
};

class McqrRunView;
class McqrSdsWidget : public McqrParamWidget
{
  Q_OBJECT

  public:
  McqrSdsWidget(McqrRunView *mcqr_window,
                const McqrExperimentSp &p_mcqr_experiment);
  virtual ~McqrSdsWidget();
  void changeStepAfterEndTag(QString end_name) override;
  void writeMcqrResultInRightFile(QString mcqr_result) override;

  protected slots:
  void analyseQualityData();
  void filteringData();
  void doFactorComboboxChanged(QString factor);
  void plotProteinHeatmapAbundance();
  void runProteinClustering();
  void runProteinAnova();
  void runProteinAbundance();
  void runProteinFcAbundance();
  void saveFinalRData();
  void changeToolBoxHeight(int tab_selected);

  private:
  void disableFutureStepsInToolBox();
  void initGrantleeEngine();
  void addFactorsToListViews();
  void configureFilteringProteinTab();
  void fillFactorsInComboboxForFC();

  private:
  Ui::McqrSdsWidget *ui;
  SdsAnalysisStep m_ongoingMcqrStep        = SdsAnalysisStep::analyzing_sc;
  Grantlee::Engine *mp_grantleeEngine      = nullptr;
  Grantlee::Template msp_rscriptTemplate   = nullptr;
  QStandardItemModel *mp_factorsColorModel = nullptr;
  QStandardItemModel *mp_factorsLabelModel = nullptr;
  QStandardItemModel *mp_factorsListModel  = nullptr;
  QStandardItemModel *mp_msrunRemoveModel  = nullptr;
  QString m_analysingScResult;
  QString m_filteringResult;
  QString m_proteinAbundanceResult;
};
