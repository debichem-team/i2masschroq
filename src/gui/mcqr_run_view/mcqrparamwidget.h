/**
 * \file gui/mcqr_run_view/mcqrparamwidget.h
 * \date 30/06/2021
 * \author Thomas Renne
 * \brief template of the Mcqr parameters for each script
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QWidget>
#include "../../core/mcqr/mcqrexperiment.h"
#include <QTemporaryDir>


class McqrRunView;
class McqrParamWidget : public QWidget
{
  Q_OBJECT

  public:
  McqrParamWidget(McqrRunView *run_view, const McqrExperimentSp &p_mcqr_exp);
  virtual ~McqrParamWidget();

  virtual void changeStepAfterEndTag(QString end_name);
  virtual void writeMcqrResultInRightFile(QString mcqr_result);
  void saveModifiedRData(QString new_rdata_path);

  protected:
  const McqrExperimentSp &getMcqrExperimentSpLoaded() const;
  QString getOutputTmpPath();
  McqrRunView *getMcqrRunParentView();
  QString transformQStringListInRListFormat(QStringList list);

  private:
  McqrRunView *mp_mcqrRunWindow = nullptr;
  // QString m_rdataPath;
  QTemporaryDir *mp_tmpPath;
  McqrExperimentSp msp_mcqrExperimentLoaded = nullptr;
};
