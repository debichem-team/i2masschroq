
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <deque>
#include <pappsomspp/msrun/msrunreader.h>
#include "../../../../utils/msrunstatisticshandler.h"
#include "core/msrun.h"
#include "core/identification_sources/identificationdatasource.h"
#include <QFuture>


class MsIdentificationTableMsRunStatisticsHandler
  : public MsRunStatisticsHandler
{
  public:
  unsigned long getMsLevelCount(unsigned int ms_level) const;
  pappso::pappso_double getMsLevelTic(unsigned int ms_level) const;


  unsigned long getTotalCount() const;

  virtual bool shouldStop() override;
  virtual void requireStop(bool stop);

  private:
  std::vector<unsigned long> _count_ms_level_spectrum;
  std::vector<pappso::pappso_double> _tic_ms_level_spectrum;
  QMutex m_mutex;

  bool m_stopRequired = false;
};

class MsIdentificationTableModel;

enum class WorkerStatus : std::int8_t
{
  Running      = 0,
  Waiting      = 1,
  FileNotFound = 2,
  Ready        = 3,
  NotReady     = 4,
  Error        = 5,
};
class MsIdentificationListWindow;
class MsIdListWorkerStatus
{
  public:
  MsIdListWorkerStatus();

  virtual ~MsIdListWorkerStatus();

  /** @brief ask to the worker if this identification source is
   * ready/computing/on hold/error ...
   */
  WorkerStatus getStatus(IdentificationDataSourceSp identificationDataSource);
  void doComputeMsNumber(IdentificationDataSourceSp identificationDataSource);
  void changeWaitingQueue(int row);


  void stopThreads();

  private:
  QFuture<void> m_computingThread;
  MsIdentificationTableMsRunStatisticsHandler *mp_handler = nullptr;
  std::deque<IdentificationDataSourceSp> m_waitingComputing;
  IdentificationDataSource *m_runningData;
  std::vector<IdentificationDataSource *> m_identificationReady;
  std::vector<IdentificationDataSource *> m_identificationError;
  std::vector<IdentificationDataSource *> m_identificationFileNotFound;
  MsIdentificationTableMsRunStatisticsHandler *mpa_currentHandler = nullptr;
};
