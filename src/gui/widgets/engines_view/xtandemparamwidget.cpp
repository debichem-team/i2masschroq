
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xtandemparamwidget.h"

#include "../../project_view/projectwindow.h"
#include "ui_xtandem_view_widget.h"
#include <QDebug>
#include <files/tandemparametersfile.h>


XtandemParamWidget::XtandemParamWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::XtandemParamWidget)
{
  qDebug() << "begin";
  ui->setupUi(this);

  qDebug() << "end";
}

XtandemParamWidget::~XtandemParamWidget()
{
  qDebug() << "XtandemParamWidget::~XtandemParamWidget";
  delete ui;
  qDebug() << "end";
}

void
XtandemParamWidget::setAutomaticXTandemParameters(
  TandemParameters xtandem_parameters)
{
  qDebug() << "begin ";
  setSpectrumParameters(xtandem_parameters);
  setProteinParameters(xtandem_parameters);
  setResidueParameters(xtandem_parameters);
  setScoringParameters(xtandem_parameters);
  setRefineParameters(xtandem_parameters);
  setOutputParameters(xtandem_parameters);

  qDebug() << "end ";
}

void
XtandemParamWidget::doHelp()
{
  QObject *senderObj    = sender();
  QString senderObjName = senderObj->objectName();
  qDebug() << "XtandemParamWidget::doHelp begin " << senderObjName;
  QFile html_doc;
  if(senderObjName == "spmmeu_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/spmmeu.html");
    }
  if(senderObjName == "smpc_push_button")
    {
      // spmmeu.html
      html_doc.setFileName(":/tandem/resources/html_doc/smpc.html");
    }
  if(senderObjName == "spmmem_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmem.html");
    }
  if(senderObjName == "spmmep_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmep.html");
    }
  if(senderObjName == "spmmie_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spmmie.html");
    }
  if(senderObjName == "sfmt_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmt.html");
    }
  if(senderObjName == "sfmmeu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmmeu.html");
    }
  if(senderObjName == "sfmme_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sfmme.html");
    }
  if(senderObjName == "sunlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sunlw.html");
    }
  if(senderObjName == "snlm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlm.html");
    }
  if(senderObjName == "snlw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/snlw.html");
    }
  if(senderObjName == "sdr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sdr.html");
    }
  if(senderObjName == "stp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/stp.html");
    }
  if(senderObjName == "smp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smp.html");
    }
  if(senderObjName == "smfmz_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smfmz.html");
    }
  if(senderObjName == "smpmh_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smpmh.html");
    }
  if(senderObjName == "spsbs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/spsbs.html");
    }
  if(senderObjName == "suca_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/suca.html");
    }
  if(senderObjName == "st_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/st.html");
    }
  if(senderObjName == "pcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcs.html");
    }
  if(senderObjName == "pcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcsemi.html");
    }

  if(senderObjName == "pcctmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcctmc.html");
    }
  if(senderObjName == "pcntmc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pcntmc.html");
    }
  if(senderObjName == "pctrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pctrmm.html");
    }
  if(senderObjName == "pntrmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pntrmm.html");
    }
  if(senderObjName == "pqa_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqa.html");
    }
  if(senderObjName == "pqp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pqp.html");
    }
  if(senderObjName == "pstpb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pstpb.html");
    }
  if(senderObjName == "pmrmf_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/pmrmf.html");
    }
  if((senderObjName == "rmm_push_button") ||
     (senderObjName == "rmm1_push_button") ||
     (senderObjName == "rmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rmm.html");
    }
  if(senderObjName == "rpmm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmm.html");
    }
  if(senderObjName == "rpmmotif_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpmmotif.html");
    }
  if(senderObjName == "smic_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smic.html");
    }
  if(senderObjName == "smmcs_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/smmcs.html");
    }
  if(senderObjName == "scp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/scp.html");
    }
  if(senderObjName == "sir_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sir.html");
    }
  if(senderObjName == "syi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/syi.html");
    }
  if(senderObjName == "sbi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sbi.html");
    }
  if(senderObjName == "sci_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sci.html");
    }
  if(senderObjName == "szi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/szi.html");
    }
  if(senderObjName == "sai_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sai.html");
    }
  if(senderObjName == "sxi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/sxi.html");
    }
  if(senderObjName == "refine_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refine.html");
    }
  if(senderObjName == "rmvev_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rmvev.html");
    }
  if(senderObjName == "refpntm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpntm.html");
    }
  if(senderObjName == "refpctm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpctm.html");
    }
  if((senderObjName == "refmm_push_button") ||
     (senderObjName == "refmm1_push_button") ||
     (senderObjName == "refmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refmm.html");
    }
  if((senderObjName == "refpmm_push_button") ||
     (senderObjName == "refpmm1_push_button") ||
     (senderObjName == "refpmm2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmm.html");
    }
  if((senderObjName == "refpmmotif_push_button") ||
     (senderObjName == "refpmmotif1_push_button") ||
     (senderObjName == "refpmmotif2_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/refpmmotif.html");
    }
  if(senderObjName == "rupmffr_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rupmffr.html");
    }
  if(senderObjName == "rcsemi_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rcsemi.html");
    }
  if(senderObjName == "ruc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ruc.html");
    }
  if(senderObjName == "rss_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rss.html");
    }
  if(senderObjName == "rpm_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/rpm.html");
    }
  if((senderObjName == "omvev_push_button") ||
     (senderObjName == "omvpev_push_button"))
    {
      html_doc.setFileName(":/tandem/resources/html_doc/omvev.html");
    }
  if(senderObjName == "oresu_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oresu.html");
    }
  if(senderObjName == "osrb_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/osrb.html");
    }
  if(senderObjName == "oprot_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oprot.html");
    }
  if(senderObjName == "oseq_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oseq.html");
    }
  if(senderObjName == "oosc_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oosc.html");
    }
  if(senderObjName == "ospec_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ospec.html");
    }
  if(senderObjName == "opara_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/opara.html");
    }
  if(senderObjName == "ohist_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohist.html");
    }
  if(senderObjName == "ohcw_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ohcw.html");
    }
  if(senderObjName == "oph_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oph.html");
    }
  if(senderObjName == "oxp_push_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/oxp.html");
    }
  if(senderObjName == "ttfparam_button")
    {
      html_doc.setFileName(":/tandem/resources/html_doc/ttfparam.html");
    }
  if(html_doc.open(QFile::ReadOnly | QFile::Text))
    {
      QTextStream in(&html_doc);
      ui->textEdit->setHtml(in.readAll());
      qDebug() << "EditTandemPresetDialog::doHelp doc " << in.readAll();
    }
  else
    {
      qDebug() << "EditTandemPresetDialog::doHelp doc not found";
    }
  qDebug() << "EditTandemPresetDialog::doHelp end " << senderObjName;
}

void
XtandemParamWidget::setSpectrumParameters(TandemParameters xtandem_parameters)
{
  ui->parenttIonMHMassToleranceWindowUnitLineEdit->setText(
    xtandem_parameters.getValue(
      "spectrum, parent monoisotopic mass error units"));
  ui->parentIonMHMassToleranceLowerWindowLineEdit->setText(
    xtandem_parameters.getValue(
      "spectrum, parent monoisotopic mass error minus"));
  ui->parentIonMHMassToleranceUpperWindowLineEdit->setText(
    xtandem_parameters.getValue(
      "spectrum, parent monoisotopic mass error plus"));
  ui->anticipateCarbonIsotopeParentIonAssignmentsErrorsLineEdit->setText(
    xtandem_parameters.getValue(
      "spectrum, parent monoisotopic mass isotope error"));
  ui->setTheMaximumPrecursorChargeToBeScoredLineEdit->setText(
    xtandem_parameters.getValue("spectrum, maximum parent charge"));
  ui->useChemicalAverageOrMonoisotopicMassForFragmentIonsLineEdit->setText(
    xtandem_parameters.getValue("spectrum, fragment mass type"));
  ui->unitsForFragmentIonMassToleranceMonoisotopicLineEdit->setText(
    xtandem_parameters.getValue("spectrum, fragment monoisotopic mass error"));
  ui->fragmentIonMassToleranceMonoisotopicLineEdit->setText(
    xtandem_parameters.getValue(
      "spectrum, parent monoisotopic mass error minus"));
  ui->controlTheUseOfTheNeutralLossWindowLineEdit->setText(
    xtandem_parameters.getValue("spectrum, use neutral loss window"));
  ui->neutralLossMassLineEdit->setText(
    xtandem_parameters.getValue("spectrum, neutral loss mass"));
  ui->neutralLossWindowDaltonsLineEdit->setText(
    xtandem_parameters.getValue("spectrum, neutral loss window"));
  ui->setTheDynamicRangeToScoreSpectraLineEdit->setText(
    xtandem_parameters.getValue("spectrum, dynamic range"));
  ui->maximumNumberOfPeaksToUseInASpectrumLineEdit->setText(
    xtandem_parameters.getValue("spectrum, total peaks"));
  ui->minimumNumberOfPeaksForASpectrumToBeConsideredLineEdit->setText(
    xtandem_parameters.getValue("spectrum, minimum peaks"));
  ui->minimumMZFragmentToBeConsideredLineEdit->setText(
    xtandem_parameters.getValue("spectrum, minimum fragment mz"));
  ui->minimumParentMHToBeConsideredLineEdit->setText(
    xtandem_parameters.getValue("spectrum, minimum parent m+h"));
  ui->alterHowProteinSequencesAreRetrievedFromFASTAFilesLineEdit->setText(
    xtandem_parameters.getValue("spectrum, sequence batch size"));
  ui->useContrastAngleLineEdit->setText(
    xtandem_parameters.getValue("spectrum, use contrast angle"));
  ui->setNumberOfThreadsLineEdit->setText(
    xtandem_parameters.getValue("spectrum, threads"));
  ui->ttfparam_edit->setText(
    xtandem_parameters.getValue("spectrum, timstof MS2 filters"));
}

void
XtandemParamWidget::setProteinParameters(TandemParameters xtandem_parameters)
{
  ui->defineProteinCleavageSiteLineEdit->setText(
    xtandem_parameters.getValue("protein, cleavage site"));
  ui->useSemiEnzymaticCleavageRulesLineEdit->setText(
    xtandem_parameters.getValue("protein, cleavage semi"));
  ui->moietyAddedToPeptideCTerminusByCleavageLineEdit->setText(
    xtandem_parameters.getValue("protein, cleavage C-terminal mass change"));
  ui->moietyAddedToPeptideNTerminusByCleavageLineEdit->setText(
    xtandem_parameters.getValue("protein, cleavage N-terminal mass change"));
  ui->moietyAddedToProteinCTerminusLineEdit->setText(
    xtandem_parameters.getValue(
      "protein, C-terminal residue modification mass"));
  ui->moietyAddedToProteinNTerminusLineEdit->setText(
    xtandem_parameters.getValue(
      "protein, N-terminal residue modification mass"));
  ui->proteinNTerminusCommonModificationsLineEdit->setText(
    xtandem_parameters.getValue("protein, quick acetyl"));
  ui->proteinNTerminusCyclisationLineEdit->setText(
    xtandem_parameters.getValue("protein, quick pyrolidone"));
  ui->interpretationOfPeptidePhosphorylationModelLineEdit->setText(
    xtandem_parameters.getValue("protein, stP bias"));
  ui->modifyTheResidueMassesOfOneAllAminoAcidsLineEdit->setText(
    xtandem_parameters.getValue("protein, modified residue mass file"));
}

void
XtandemParamWidget::setResidueParameters(TandemParameters xtandem_parameters)
{
  ui->fixedModificationsLineEdit->setText(
    xtandem_parameters.getValue("residue, modification mass"));
  ui->fixedModifications1LineEdit->setText(
    xtandem_parameters.getValue("residue, modification mass 1"));
  ui->fixedModifications2LineEdit->setText(
    xtandem_parameters.getValue("residue, modification mass 2"));

  ui->potentialModificationsLineEdit->setText(
    xtandem_parameters.getValue("residue, potential modification mass"));
  ui->potentialModificationsMotifLineEdit->setText(
    xtandem_parameters.getValue("residue, potential modification motif"));
}

void
XtandemParamWidget::setScoringParameters(TandemParameters xtandem_parameters)
{
  ui->minimumNumberOfIonsToScoreAPeptideLineEdit->setText(
    xtandem_parameters.getValue("scoring, minimum ion count"));
  ui->maximumMissedCleavageSitesLineEdit->setText(
    xtandem_parameters.getValue("scoring, maximum missed cleavage sites"));
  ui->compensateForVerySmallSequenceListFileLineEdit->setText(
    xtandem_parameters.getValue("scoring, cyclic permutation"));
  ui->automaticallyPerformReverseDatabaseSearchLineEdit->setText(
    xtandem_parameters.getValue("scoring, include reverse"));
  ui->useYIonsLineEdit->setText(xtandem_parameters.getValue("scoring, y ions"));
  ui->useBIonsLineEdit->setText(xtandem_parameters.getValue("scoring, b ions"));
  ui->useCIonsLineEdit->setText(xtandem_parameters.getValue("scoring, c ions"));
  ui->useZIonsLineEdit->setText(xtandem_parameters.getValue("scoring, z ions"));
  ui->useAIonsLineEdit->setText(xtandem_parameters.getValue("scoring, a ions"));
  ui->useXIonsLineEdit->setText(xtandem_parameters.getValue("scoring, x ions"));
}

void
XtandemParamWidget::setRefineParameters(TandemParameters xtandem_parameters)
{
  ui->useOfTheRefinementModuleLineEdit->setText(
    xtandem_parameters.getValue("refine"));
  ui->maximumEValueToAcceptPeptideLineEdit->setText(
    xtandem_parameters.getValue("refine, maximum valid expectation value"));

  ui->peptideNTerPotentialModificationsLineEdit->setText(
    xtandem_parameters.getValue("refine, potential N-terminus modifications"));
  ui->peptideCTerPotentialModificationsLineEdit->setText(
    xtandem_parameters.getValue("refine, potential C-terminus modifications"));

  ui->alterFixedModificationsLineEdit->setText(
    xtandem_parameters.getValue("refine, modification mass"));
  ui->alterFixedModifications1LineEdit->setText(
    xtandem_parameters.getValue("refine, modification mass 1"));
  ui->alterFixedModifications2LineEdit->setText(
    xtandem_parameters.getValue("refine, modification mass 2"));

  ui->alterPotentialModificationsLineEdit->setText(
    xtandem_parameters.getValue("refine, potential modification mass"));
  ui->alterPotentialModifications1LineEdit->setText(
    xtandem_parameters.getValue("refine, potential modification mass 1"));
  ui->alterPotentialModifications2LineEdit->setText(
    xtandem_parameters.getValue("refine, potential modification mass 2"));

  ui->potentialModificationsMotifLineEdit_2->setText(
    xtandem_parameters.getValue("refine, potential modification motif"));
  ui->potentialModificationsMotif1LineEdit->setText(
    xtandem_parameters.getValue("refine, potential modification motif 1"));
  ui->potentialModificationsMotif2LineEdit->setText(
    xtandem_parameters.getValue("refine, potential modification motif 2"));

  ui->usePotentialModificationsInRefinementModuleLineEdit->setText(
    xtandem_parameters.getValue(
      "refine, use potential modifications for full refinement"));
  ui->useSemiEnzymaticCleavageRulesLineEdit_2->setText(
    xtandem_parameters.getValue("refine, cleavage semi"));
  ui->cleavageAtEachResiduesLineEdit->setText(
    xtandem_parameters.getValue("refine, unanticipated cleavage"));
  ui->spectrumIntensityModelSynthesisLineEdit->setText(
    xtandem_parameters.getValue("refine, spectrum synthesis"));
  ui->pointMutationsLineEdit->setText(
    xtandem_parameters.getValue("refine, point mutations"));
}

void
XtandemParamWidget::setOutputParameters(TandemParameters xtandem_parameters)
{
  ui->highestEValueToRecordPeptidesLineEdit->setText(
    xtandem_parameters.getValue("output, maximum valid expectation value"));
  ui->highestEValueToRecordProteinsLineEdit->setText(
    xtandem_parameters.getValue(
      "output, maximum valid protein expectation value"));
  ui->resultsTypeLineEdit->setText(
    xtandem_parameters.getValue("output, results"));
  ui->sortResultsByLineEdit->setText(
    xtandem_parameters.getValue("output, sort results by"));
  ui->writeProteinsLineEdit->setText(
    xtandem_parameters.getValue("output, proteins"));
  ui->writeProteinSequencesLineEdit->setText(
    xtandem_parameters.getValue("output, sequences"));
  ui->writeOnlyFirstProteinSequenceLineEdit->setText(
    xtandem_parameters.getValue("output, one sequence copy"));
  ui->writeSpectrumInformationsLineEdit->setText(
    xtandem_parameters.getValue("output, spectra"));
  ui->writeParametersLineEdit->setText(
    xtandem_parameters.getValue("output, parameters"));
  ui->writeHistogramsLineEdit->setText(
    xtandem_parameters.getValue("output, histograms"));
  ui->histogramColumnWidthLineEdit->setText(
    xtandem_parameters.getValue("output, histogram column width"));
  ui->hashFilenameWithDateAndTimeStampLineEdit->setText(
    xtandem_parameters.getValue("output, path hashing"));
  ui->xSLTStyleSheetPathToViewXMLResultsLineEdit->setText(
    xtandem_parameters.getValue("output, xsl path"));
}
