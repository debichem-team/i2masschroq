/**
 * \file src/gui/widgets/qcp_massdelta/qcpmassdelta.h
 * \date 20/03/2021
 * \author Olivier Langella
 * \brief customized object replacing QCustomPlot
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <qcustomplot.h>
#include "../../../core/peptideevidence.h"
#include "massdeltawidget.h"

/**
 * @todo write docs
 */
class QCPMassDelta : public QCustomPlot
{
  Q_OBJECT
  public:
  /**
   * Default constructor
   */
  QCPMassDelta(QWidget *parent);

  /**
   * Destructor
   */
  ~QCPMassDelta();

  virtual void mouseMoveEvent(QMouseEvent *event) override;

  void setMassDeltaWidget(MassDeltaWidget *p_mass_delta_widget);

  void reset();

  void
  setPeptideEvidenceList(const Project *p_project,
                         const std::vector<std::shared_ptr<PeptideEvidence>>
                           &peptide_evidence_list_in);

  void setCurrentPeptideEvidenceSp(
    std::shared_ptr<PeptideEvidence> &currentPeptideEvidence);

  protected:
  void mousePressEvent(QMouseEvent *event) override;

  private:
  double rankToMass(double rank,
                    const QCPDataContainer<QCPGraphData> &container);

  QCPDataContainer<QCPGraphData>::const_iterator m_beginSelect;
  QCPDataContainer<QCPGraphData>::const_iterator m_endSelect;

  // double m_currentMass                                        = 0;
  std::shared_ptr<PeptideEvidence> msp_currentPeptideEvidence = nullptr;

  std::vector<std::shared_ptr<PeptideEvidence>> m_peptideEvidenceSpList;

  QCPItemText *m_peptideLabel;
  QCPItemStraightLine *m_massDeltaLine;
  QCPItemRect *m_precisionRange;

  pappso::PrecisionPtr mp_precision;

  pappso::MzRange *mpa_currentMassRange = nullptr;

  MassDeltaWidget *mp_massDeltaWidget = nullptr;
};
