/**
 * \file gui/widget/xic_extraction_method_widget/xicextractionmethodwidget.cpp
 * \date 17/02/2019
 * \author Olivier Langella
 * \brief choose Xic extraction method (sum or max)
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicextractionmethodwidget.h"
#include <QDebug>
#include <QHBoxLayout>
#include <QSettings>

XicExtractionMethodWidget::XicExtractionMethodWidget(QWidget *parent)
  : QWidget(parent)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  setLayout(new QHBoxLayout(this));

  this->layout()->setMargin(0);
  this->layout()->setContentsMargins(0, 0, 0, 0);

  mp_methodComboBox = new QComboBox();
  this->layout()->addWidget(mp_methodComboBox);

  mp_methodComboBox->addItem("max", QString("max"));
  mp_methodComboBox->addItem("sum", QString("sum"));

  QSettings settings;

  m_oldMethod =
    (pappso::XicExtractMethod)settings
      .value("xic/xic_extraction_method",
             QString("%1").arg((std::uint8_t)pappso::XicExtractMethod::max))
      .toUInt();

  if((m_oldMethod != pappso::XicExtractMethod::max) &&
     ((m_oldMethod != pappso::XicExtractMethod::sum)))
    {
      m_oldMethod = pappso::XicExtractMethod::max;
    }

  if(m_oldMethod == pappso::XicExtractMethod::max)
    {
      mp_methodComboBox->setCurrentIndex(0);
    }

  if(m_oldMethod == pappso::XicExtractMethod::sum)
    {
      mp_methodComboBox->setCurrentIndex(1);
    }
  connect(mp_methodComboBox,
          SIGNAL(currentIndexChanged(int)),
          this,
          SLOT(setCurrentIndex(int)));


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

XicExtractionMethodWidget::~XicExtractionMethodWidget()
{
}

void
XicExtractionMethodWidget::setCurrentIndex(int index)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << index;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << (int)m_oldMethod;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << (int)pappso::XicExtractMethod::max;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << (int)pappso::XicExtractMethod::sum;
  pappso::XicExtractMethod new_method = pappso::XicExtractMethod::max;
  if(index == 0)
    {
      new_method = pappso::XicExtractMethod::max;
    }
  if(index == 1)
    {
      new_method = pappso::XicExtractMethod::sum;
    }


  if(m_oldMethod != new_method)
    {
      m_oldMethod = new_method;
      QSettings settings;
      settings.setValue("xic/xic_extraction_method",
                        QString("%1").arg((std::uint8_t)new_method));
      emit xicExtractionMethodChanged(new_method);
    }
}


pappso::XicExtractMethod
XicExtractionMethodWidget::getXicExtractionMethod() const
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << (int)m_oldMethod;
  return m_oldMethod;
}
