
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "automaticfilterwidget.h"

#include "../../project_view/projectwindow.h"
#include "ui_automatic_filter_widget.h"
#include <QDebug>
#include <cmath>


AutomaticFilterWidget::AutomaticFilterWidget(QWidget *parent)
  : QWidget(parent), ui(new Ui::AutomaticFilterWidget)
{
  qDebug() << "AutomaticFilterWidget::AutomaticFilterWidget begin";
  ui->setupUi(this);

  qDebug() << "AutomaticFilterWidget::AutomaticFilterWidget end";
}

AutomaticFilterWidget::~AutomaticFilterWidget()
{
  qDebug() << "AutomaticFilterWidget::~AutomaticFilterWidget";
  delete ui;
  qDebug() << "AutomaticFilterWidget::~AutomaticFilterWidget end";
}


AutomaticFilterParameters
AutomaticFilterWidget::getAutomaticFilterParameters() const
{
  return _parameters;
}

void
AutomaticFilterWidget::setAutomaticFilterParameters(
  const AutomaticFilterParameters &params)
{
  qDebug() << "AutomaticFilterWidget::setAutomaticFilterParameters begin ";
  _parameters   = params;
  _emit_changed = false;
  ui->evalue_radio_button->setChecked(true);

  ui->peptide_fdr_spinbox->setDisabled(true);
  ui->peptide_evalue_spinbox->setEnabled(true);
  if(_parameters.useFDR())
    {
      ui->fdr_radio_button->setChecked(true);

      ui->peptide_evalue_spinbox->setDisabled(true);
      ui->peptide_fdr_spinbox->setEnabled(true);
    }
  ui->peptide_evalue_spinbox->setValue(_parameters.getFilterPeptideEvalue());
  ui->protein_evalue_spinbox->setValue(_parameters.getFilterProteinEvalue());
  ui->protein_evalue_log_spinbox->setValue(
    std::log10(_parameters.getFilterProteinEvalue()));

  ui->peptide_number_spinbox->setValue(
    _parameters.getFilterMinimumPeptidePerMatch());
  // ui->protein_evalue_log_spinbox->setValue(std::log(_parameters.getFilterProteinEvalue()));
  ui->cross_sample_checkbox->setCheckState(Qt::Unchecked);
  if(_parameters.getFilterCrossSamplePeptideNumber())
    ui->cross_sample_checkbox->setCheckState(Qt::Checked);
  _emit_changed = true;
  qDebug() << "AutomaticFilterWidget::setAutomaticFilterParameters end ";
}

void
AutomaticFilterWidget::doCrossSample(bool is_cross_sample)
{
  _parameters.setFilterCrossSamplePeptideNumber(is_cross_sample);

  if(_emit_changed)
    emit automaticFilterParametersChanged(_parameters);
}
void
AutomaticFilterWidget::doPeptideEvalue(double evalue)
{
  _parameters.setFilterPeptideEvalue(evalue);

  if(_emit_changed)
    emit automaticFilterParametersChanged(_parameters);
}
void
AutomaticFilterWidget::doProteinEvalue(double evalue)
{
  if(_signal)
    {
      _parameters.setFilterProteinEvalue(evalue);
      _signal = false;
      ui->protein_evalue_log_spinbox->setValue(
        std::log10(_parameters.getFilterProteinEvalue()));

      if(_emit_changed)
        emit automaticFilterParametersChanged(_parameters);
    }
  else
    {
      _signal = true;
    }
}
void
AutomaticFilterWidget::doProteinLogEvalue(double evalue)
{
  if(_signal)
    {
      _parameters.setFilterProteinEvalue(std::pow(10, evalue));
      _signal = false;
      ui->protein_evalue_spinbox->setValue(
        _parameters.getFilterProteinEvalue());

      if(_emit_changed)
        emit automaticFilterParametersChanged(_parameters);
    }
  else
    {
      _signal = true;
    }
}
void
AutomaticFilterWidget::doPeptideNumber(int number)
{
  _parameters.setFilterMinimumPeptidePerMatch((unsigned int)number);
  if(_emit_changed)
    emit automaticFilterParametersChanged(_parameters);
}

void
AutomaticFilterWidget::doPepreproChanged(int number)
{
  _parameters.setFilterPeptideObservedInLessSamplesThan((unsigned int)number);
  if(_emit_changed)
    emit automaticFilterParametersChanged(_parameters);
}

void
AutomaticFilterWidget::doValueChanged(double value [[maybe_unused]])
{
  doSetParameters();
}
void
AutomaticFilterWidget::doSetParameters()
{
  if(ui->fdr_radio_button->isChecked())
    {
      _parameters.setFilterPeptideFDR(ui->peptide_fdr_spinbox->value() / 100);
      ui->peptide_evalue_spinbox->setDisabled(true);
      ui->peptide_fdr_spinbox->setEnabled(true);
    }
  else
    {
      _parameters.setFilterPeptideFDR(-1);
      ui->peptide_fdr_spinbox->setDisabled(true);
      ui->peptide_evalue_spinbox->setEnabled(true);
    }
  if(_emit_changed)
    emit automaticFilterParametersChanged(_parameters);
}


int
AutomaticFilterWidget::getAutomaticFilterWindowWidth() const
{
  return ui->gridLayout_2->minimumSize().width();
}
