/**
 * \file gui/mcqr_param_dialog/mcqrparamallquantidialog.h
 * \date 11/01/2021
 * \author Thomas Renne
 * \brief set MCQR params and run
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QDialog>
#include <QStandardItemModel>
#include "../../core/project.h"


namespace Ui
{
class McqrParamAllQuantiDialog;
}


class MainWindow;

class McqrParamAllQuantiDialog : public QDialog
{
  Q_OBJECT

  public:
  explicit McqrParamAllQuantiDialog(MainWindow *parent,
                                    ProjectSp project,
                                    QString masschroqml_name);
  explicit McqrParamAllQuantiDialog(MainWindow *parent, ProjectSp project);

  ~McqrParamAllQuantiDialog();
  void reject() override;

  // QString getMasschroqmlPath();
  bool getMcqrRunStatus();
  bool getMassChroqResultsUsed();

  const QFileInfo &getMetadataOdsFileInfo() const;

  protected slots:
  void doDownloadMetadataTemplate();
  void loadFilledMetadataOds();
  void handleFractionNameEnabling();
  void doSaveFullRDataFile();
  void doRunMcqrWithRData();

  private:
  void fillMetadataTableView();
  void setMetadataViewColNames();
  void setMetadataModelView();
  void setMcqrExpSummary();

  /** @brief find the alignment group corresponding to the masschroq file
   * @return MsRunAlignmentGroupSp sharde pointer on group
   */
  MsRunAlignmentGroupSp checkMasschroqmlPath(QString masschroqml_name);
  McqrLoadDataMode getMcqrLoadDataMode();

  private:
  Ui::McqrParamAllQuantiDialog *ui;
  MainWindow *mp_main;
  ProjectSp msp_project;
  MsRunAlignmentGroupSp msp_mcqMsrunAlignementGroup;
  bool m_runMCQR       = false;
  bool m_masschroqUsed = false;
  QStandardItemModel *mp_metadataModel;
  QFileInfo m_metadataOdsFile;
};
