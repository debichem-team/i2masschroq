/**
 * \file gui/export/export_spreadsheet_dialog/exportspreadsheetdialog.cpp
 * \date 11/4/2017
 * \author Olivier Langella
 * \brief choose ODS export options
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ui_export_spreadsheet_dialog.h"
#include "exportspreadsheetdialog.h"
#include <QDebug>
#include <QSettings>

ExportSpreadsheetDialog::ExportSpreadsheetDialog(QWidget *parent)
  : QDialog(parent), ui(new Ui::ExportSpreadsheetDialog)
{
  qDebug() << "ExportSpreadsheetDialog::ExportSpreadsheetDialog begin";
  ui->setupUi(this);
  this->setModal(true);


  QSettings settings;

  ui->groups_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/groups", "true").toBool())
    {
      ui->groups_checkbox->setCheckState(Qt::Checked);
    }
  // ui->simple_checkbox->setVisible(false);
  // ui->simple_checkbox->setCheckState(Qt::Unchecked);
  // if (settings.value("export_ods/simple", "false").toBool()) {
  //    ui->simple_checkbox->setCheckState(Qt::Checked);
  //}
  ui->protein_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/proteins", "true").toBool())
    {
      ui->protein_checkbox->setCheckState(Qt::Checked);
    }
  ui->peptide_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/peptides", "true").toBool())
    {
      ui->peptide_checkbox->setCheckState(Qt::Checked);
    }
  ui->spectra_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/spectra", "true").toBool())
    {
      ui->spectra_checkbox->setCheckState(Qt::Checked);
    }

  if(settings.value("export_ods/spectra_mobility_informations", "false")
       .toBool())
    {
      ui->spectra_mobility_information_checkbox->setCheckState(Qt::Checked);
    }
  ui->psm_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/psm", "true").toBool())
    {
      ui->psm_checkbox->setCheckState(Qt::Checked);
    }
  ui->peptidepos_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/peptidepos", "true").toBool())
    {
      ui->peptidepos_checkbox->setCheckState(Qt::Checked);
    }

  ui->comparspectra_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparspectra", "true").toBool())
    {
      ui->comparspectra_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparspecificspectra_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparspecificspectra", "true").toBool())
    {
      ui->comparspecificspectra_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparuniquesequence_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparuniquesequence", "true").toBool())
    {
      ui->comparuniquesequence_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparspecificuniquesequence_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparspecificuniquesequence", "true").toBool())
    {
      ui->comparspecificuniquesequence_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparpai_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparpai", "true").toBool())
    {
      ui->comparpai_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparempai_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparempai", "true").toBool())
    {
      ui->comparempai_checkbox->setCheckState(Qt::Checked);
    }
  ui->comparnsaf_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/comparnsaf", "true").toBool())
    {
      ui->comparnsaf_checkbox->setCheckState(Qt::Checked);
    }
  ui->samples_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/samples", "true").toBool())
    {
      ui->samples_checkbox->setCheckState(Qt::Checked);
    }
  ui->ptmislands_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/ptmislands", "true").toBool())
    {
      ui->ptmislands_checkbox->setCheckState(Qt::Checked);
    }
  ui->ptmspectra_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/ptmspectra", "true").toBool())
    {
      ui->ptmspectra_checkbox->setCheckState(Qt::Checked);
    }
  // param.setFilterCrossSamplePeptideNumber(settings.value("automatic_filter/cross_sample",
  // "true").toBool());

  if(settings.value("export_ods/format", "ods").toString() == "ods")
    {
      ui->ods_radiobutton->setChecked(true);
    }
  else
    {
      ui->tsv_radiobutton->setChecked(true);
    }

  ui->peptidomiccomparspectra_checkbox->setCheckState(Qt::Unchecked);
  if(settings.value("export_ods/peptidomiccomparspectra", "false").toBool())
    {
      ui->peptidomiccomparspectra_checkbox->setCheckState(Qt::Checked);
    }

#if QT_VERSION >= 0x050000
    // Qt5 code
#else
    // Qt4 code

#endif

  qDebug() << "ExportSpreadsheetDialog::ExportSpreadsheetDialog end";
}

ExportSpreadsheetDialog::~ExportSpreadsheetDialog()
{
  delete ui;
}


void
ExportSpreadsheetDialog::doPeptides(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/peptides", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doProteins(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/proteins", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doSamples(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/samples", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doSpectra(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/spectra", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doSpectraIonMobilityInformations(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/spectra_mobility_informations",
                    QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doPsms(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/psm", QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doComparSpectra(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparspectra", QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doComparSpecificSpectra(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparspecificspectra",
                    QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doPeptidomicComparSpectra(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/peptidomiccomparspectra",
                    QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doComparUniqueSequence(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparuniquesequence",
                    QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doComparSpecificUniqueSequence(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparspecificuniquesequence",
                    QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doComparEmpai(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparempai", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doComparPai(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparpai", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doComparNsaf(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/comparnsaf", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doPeptidePos(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/peptidepos", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doGroups(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/groups", QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doSimpleProteinPeptideList(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/simple", QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::doProteinList(bool simple)
{

  QSettings settings;
  settings.setValue("export_ods/proteins", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doPtmIslands(bool simple)
{
  QSettings settings;
  settings.setValue("export_ods/ptmislands", QString("%1").arg(simple));
}
void
ExportSpreadsheetDialog::doPtmSpectra(bool simple)
{
  QSettings settings;
  settings.setValue("export_ods/ptmspectra", QString("%1").arg(simple));
}

void
ExportSpreadsheetDialog::setProject(const Project *p_project)
{

  if(p_project->getProjectMode() == ProjectMode::individual)
    {
      ui->compar_groupbox->setDisabled(true);
    }
  else
    {
      ui->compar_groupbox->setDisabled(false);
    }

  if(p_project->hasPtmExperiment())
    {
      ui->ptm_groupbox->setEnabled(true);
    }
  else
    {
      ui->ptm_groupbox->setEnabled(false);
    }
}

QString
ExportSpreadsheetDialog::getExportFormat() const
{
  QSettings settings;
  QString format = "ods";
  if(ui->tsv_radiobutton->isChecked())
    {
      format = "tsv";
    }
  settings.setValue("export_ods/format", format);
  return format;
}
