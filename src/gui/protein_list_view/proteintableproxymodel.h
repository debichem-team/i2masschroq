
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PROTEINTABLEPROXYMODEL_H
#define PROTEINTABLEPROXYMODEL_H

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include "../../core/project.h"
#include "proteintablemodel.h"
#include "../widgets/massitemdelegate.h"

class ProteinListWindow;

class ProteinTableModel;

class ProteinTableProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
  ProteinTableProxyModel(ProteinListWindow *p_protein_list_window,
                         ProteinTableModel *protein_table_model_p);
  virtual ~ProteinTableProxyModel();

  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;
  bool filterAcceptsColumn(int source_column,
                           const QModelIndex &source_parent) const override;

  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;
  bool lessThan(const QModelIndex &left,
                const QModelIndex &right) const override;

  void hideNotValid(bool hide);
  void hideNotChecked(bool hide);
  void hideNotGrouped(bool hide);
  void setProteinSearchString(QString protein_search_string);
  void setMsrunFileSearch(QString msrun_file_search);
  void setSearchOn(QString search_on);
  void setProteinListColumnDisplay(ProteinListColumn column, bool toggled);
  bool getProteinListColumnDisplay(ProteinListColumn column) const;
  void resteItemDelegates() const;

  public slots:
  void onTableClicked(const QModelIndex &index);


  private:
  ProteinTableModel *_protein_table_model_p;
  ProteinListWindow *_p_protein_list_window;
  QString _search_on = "Accession";
  QString _protein_search_string;
  bool _hide_not_valid   = true;
  bool _hide_not_checked = true;
  bool _hide_not_grouped = true;
  std::vector<bool> _column_display;
  PercentItemDelegate *_percent_delegate;
};

#endif // PROTEINTABLEPROXYMODEL_H
