/**
 * \file gui/ptm_peptide_list_window/ptmpeptidetableproxymodel.h
 * \date 10/6/2017
 * \author Olivier Langella
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMPEPTIDETABLEPROXYMODEL_H
#define PTMPEPTIDETABLEPROXYMODEL_H

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QMenu>
#include <QIcon>
#include "../../grouping/ptm/ptmsamplescan.h"


class PtmPeptideListWindow;
class PtmPeptideTableModel;

class PtmPeptideTableProxyModel;
class PtmPeptideMenuQicon : public QIcon
{
  public:
  explicit PtmPeptideMenuQicon(
    const PtmGroupingExperiment *p_ptm_grouping_experiment,
    const PtmSampleScan *p_ptm_sample,
    const PeptideMatch *p_peptide_match);
  ~PtmPeptideMenuQicon();

  void paint(QPainter *painter,
             const QRect &rect,
             Qt::Alignment alignment = Qt::AlignCenter,
             Mode mode               = Normal,
             State state             = Off) const;

  private:
  const PtmSampleScan *_p_ptm_sample_scan;
  const PeptideMatch _peptide_match;
  const PtmGroupingExperiment *_p_ptm_grouping_experiment;
};

class PtmPeptideMenuQaction : public QAction
{
  Q_OBJECT
  public:
  explicit PtmPeptideMenuQaction(
    PtmPeptideTableProxyModel *parent,
    const PtmGroupingExperiment *p_ptm_grouping_experiment,
    const PtmSampleScan *p_ptm_sample,
    PeptideMatch *p_peptide_match);
  ~PtmPeptideMenuQaction();

  public slots:
  void doTriggered(bool triggered);

  private:
  PeptideMatch _peptide_match;
  PtmPeptideTableProxyModel *_p_ptm_peptide_table_proxy_model;
};


class PtmPeptideTableProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
  PtmPeptideTableProxyModel(PtmPeptideListWindow *p_ptm_peptide_list_window,
                            PtmPeptideTableModel *ptm_table_model_p);
  ~PtmPeptideTableProxyModel();

  PtmPeptideListWindow *getPtmPeptideListWindowP();
  bool filterAcceptsRow(int source_row,
                        const QModelIndex &source_parent) const override;
  void hideNoPtm(bool hide);

  public slots:
  void onTableClicked(const QModelIndex &index);

  protected:
  bool lessThan(const QModelIndex &left,
                const QModelIndex &right) const override;
  void showContextMenu(const QModelIndex &index);

  private:
  PtmPeptideListWindow *_p_ptm_peptide_list_window;
  PtmPeptideTableModel *_p_ptm_table_model;
  QMenu *_p_context_menu = nullptr;
  bool m_hideNoPtm       = false;
};

#endif // PTMPEPTIDETABLEPROXYMODEL_H
