/**
 * \file gui/ptm_peptide_list_window/ptmpeptidelistwindow.cpp
 * \date 10/6/2017
 * \author Olivier Langella
 * \brief display all peptides from one ptm island
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptmpeptidelistwindow.h"
#include "../ptm_island_list_view/ptmislandlistwindow.h"
#include "ptmpeptidetablemodel.h"
#include "ptmsequencedelegate.h"
#include "../project_view/projectwindow.h"

#include "ui_ptm_peptide_list_view.h"


PtmPeptideListWindow::PtmPeptideListWindow(PtmIslandListWindow *parent)
  : QMainWindow(parent), ui(new Ui::PtmPeptideListWindow)
{
  _p_ptm_island_list_window = parent;

  ui->setupUi(this);
  /*
   */
  _ptm_table_model_p = new PtmPeptideTableModel(this);


  _ptm_proxy_model_p = new PtmPeptideTableProxyModel(this, _ptm_table_model_p);
  _ptm_proxy_model_p->setSourceModel(_ptm_table_model_p);
  _ptm_proxy_model_p->setDynamicSortFilter(true);
  ui->ptm_peptide_tableview->setModel(_ptm_proxy_model_p);
  ui->ptm_peptide_tableview->setSortingEnabled(true);
  ui->ptm_peptide_tableview->setAlternatingRowColors(true);
  ui->ptm_peptide_tableview->horizontalHeader()->setSectionsMovable(true);

  PtmSequenceDelegate *p_sequence_delegate =
    new PtmSequenceDelegate(_p_ptm_island_list_window, this);
  ui->ptm_peptide_tableview->setItemDelegateForColumn(
    (std::int8_t)PtmPeptideListColumn::sequence, p_sequence_delegate);


#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(ui->ptm_peptide_tableview,
          &QTableView::clicked,
          _ptm_proxy_model_p,
          &PtmPeptideTableProxyModel::onTableClicked);
  connect(this,
          &PtmPeptideListWindow::requestPeptideDetailView,
          _p_ptm_island_list_window->getProjectWindowP(),
          &ProjectWindow::doViewPeptideDetail);
  connect(ui->actionOnly_peptide_wearing_PTM,
          &QAction::changed,
          this,
          &PtmPeptideListWindow::doShowPtmPeptides);
  connect(this,
          &PtmPeptideListWindow::ptmPeptideChanged,
          _ptm_table_model_p,
          &PtmPeptideTableModel::onPtmPeptideDataChanged);
#else
  // Qt4 code
  // connect (this, SIGNAL(ptmIslandDataChanged()), _ptm_table_model_p,
  // SLOT(onPtmIslandDataChanged()));

  // connect (_project_window,
  // SIGNAL(identificationPtmGroupGrouped(IdentificationGroup *)),
  // this,SLOT(doIdentificationPtmGroupGrouped(IdentificationGroup *))); connect
  // (_project_window, SIGNAL(identificationGroupGrouped(IdentificationGroup
  // *)), this,SLOT(doIdentificationGroupGrouped(IdentificationGroup *)));
  connect(ui->ptm_peptide_tableview,
          SIGNAL(clicked(const QModelIndex &)),
          _ptm_proxy_model_p,
          SLOT(onTableClicked(const QModelIndex &)));
  connect(this,
          SIGNAL(requestPeptideDetailView(PeptideMatch *)),
          _p_ptm_island_list_window->getProjectWindowP(),
          SLOT(doViewPeptideDetail(PeptideMatch *)));

#endif
}
PtmPeptideListWindow::~PtmPeptideListWindow()
{
}
void
PtmPeptideListWindow::setPtmIsland(PtmIsland *p_ptm_island)
{
  _p_ptm_island = p_ptm_island;
  _ptm_table_model_p->setPtmIsland(
    _p_ptm_island_list_window->getIdentificationGroup()
      ->getPtmGroupingExperiment(),
    _p_ptm_island);
}

void
PtmPeptideListWindow::doShowPtmPeptides()
{
  if(ui->actionOnly_peptide_wearing_PTM->isChecked())
    {
      _ptm_proxy_model_p->hideNoPtm(true);
    }
  else
    {
      _ptm_proxy_model_p->hideNoPtm(false);
    }
  emit ptmPeptideChanged();
}

void
PtmPeptideListWindow::askPeptideDetailView(PeptideEvidence *p_peptide_evidence)
{
  qDebug() << "PtmPeptideListWindow::askPeptideDetailView begin "
           << p_peptide_evidence;
  // PeptideMatch peptide_match(* p_peptide_match);
  // qDebug() << "PtmPeptideListWindow::askPeptideDetailView begin " <<
  // peptide_match.getPeptideEvidence();
  emit requestPeptideDetailView(p_peptide_evidence);
  qDebug() << "PtmPeptideListWindow::askPeptideDetailView end";
  // updateStatusBar();
}
