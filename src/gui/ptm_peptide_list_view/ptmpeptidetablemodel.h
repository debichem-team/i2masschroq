/**
 * \file gui/ptm_peptide_list_window/ptmpeptidetablemodel.h
 * \date 10/6/2017
 * \author Olivier Langella
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef PTMPEPTIDETABLEMODEL_H
#define PTMPEPTIDETABLEMODEL_H

#include <QAbstractTableModel>
#include "../../grouping/ptm/ptmsamplescan.h"
#include "../../grouping/ptm/ptmisland.h"


/** \def PtmPeptideListColumn list of available fields to display in ptm peptide
 * list
 *
 */

enum class PtmPeptideListColumn : std::int8_t
{
  peptide_ptm_grouping_id = 0, ///< peptide PTM grouping id
  sample                  = 1,
  scan                    = 2,
  rt                      = 3,
  charge                  = 4,
  sequence                = 5,
  modifs                  = 6,
  start                   = 7,
  length                  = 8,
  bestEvalue              = 9,
  theoretical_mhplus      = 10,
  delta_mhplus            = 11,
  besthyperscore          = 12,
  bestposition            = 13,
  allobservedposition     = 14

};


class PtmPeptideListWindow;
class PtmPeptideTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PtmPeptideTableModel(PtmPeptideListWindow *p_ptm_peptide_list_window);
  virtual int
  rowCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual int
  columnCount(const QModelIndex &parent = QModelIndex()) const override;
  virtual QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  virtual QVariant data(const QModelIndex &index,
                        int role = Qt::DisplayRole) const override;
  static const QString getTitle(PtmPeptideListColumn column);
  static const QString getDescription(PtmPeptideListColumn column);

  ~PtmPeptideTableModel();
  void setPtmIsland(const PtmGroupingExperiment *p_ptm_grouping_experiment,
                    PtmIsland *ptm_island);

  const std::vector<PtmSampleScanSp> &getPtmSampleScanSpList() const;
  const PtmGroupingExperiment *getPtmGroupingExperiment() const;

  private:
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);

  public slots:
  void onPtmPeptideDataChanged();

  private:
  std::vector<PtmSampleScanSp> _ptm_sample_scan_list;
  const PtmIsland *_p_ptm_island;
  const PtmGroupingExperiment *_p_ptm_grouping_experiment;
};

#endif // PTMPEPTIDETABLEMODEL_H
