
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <QAction>
#include "peptidetablemodel.h"
#include "peptidetableproxymodel.h"
#include "../../core/proteinmatch.h"
#include "../../core/project.h"

class ProjectWindow;
// http://doc.qt.io/qt-4.8/qt-itemviews-chart-mainwindow-cpp.html
namespace Ui
{
class PeptideView;
}


class PeptideListWindow;
class PeptideListQactionColumn : public QAction
{
  Q_OBJECT
  public:
  explicit PeptideListQactionColumn(PeptideListWindow *parent,
                                    PeptideListColumn column);
  ~PeptideListQactionColumn();

  public slots:
  void doToggled(bool toggled);

  private:
  PeptideListWindow *_p_peptide_list_window;
  PeptideListColumn _column;
};

class PeptideListWindow : public QMainWindow
{
  Q_OBJECT

  friend class PeptideTableModel;
  friend class PeptideTableProxyModel;

  public:
  explicit PeptideListWindow(ProjectWindow *parent = 0);
  ~PeptideListWindow();
  void setProteinMatch(IdentificationGroup *p_identification_group,
                       ProteinMatch *p_protein_match);
  void resizeColumnsToContents();
  void edited();
  ProjectWindow *getProjectWindow();
  bool getPeptideListColumnDisplay(PeptideListColumn column) const;
  void setPeptideListColumnDisplay(PeptideListColumn column, bool toggled);

  virtual void closeEvent(QCloseEvent *event) override;

  public slots:
  // void peptideEdited(QString peptideStr);
  // void setColor(const QColor &color);
  // void setShape(Shape shape);
  void
  doIdentificationGroupGrouped(IdentificationGroup *p_identification_group);
  void doProjectNameChanged(QString name);

  protected slots:
  void showContextMenu(const QPoint &pos);
  void doCopyPath(QAction *action);
  void doExportAsOdsFile();
  signals:
  void identificationGroupEdited(IdentificationGroup *p_identification_group);
  void peptideDataChanged();

  protected:
  void askPeptideDetailView(PeptideEvidence *p_peptide_evidence);

  protected slots:
  void doNotValidHide(bool hide);
  void doNotCheckedHide(bool hide);
  void doNotGroupedHide(bool hide);
  void doPeptideSearchEdit(QString peptide_search_string);
  void doMsrunFileSearch(QString msr_run_file_search);
  void doModificationSearch(QString mod_search);
  void doScanNumberSearch(int scan_num);
  void doSearchOn(QString search_on);
  void updateStatusBar();

  private:
  Ui::PeptideView *ui;
  PeptideTableModel *_peptide_table_model_p = nullptr;
  PeptideTableProxyModel *_p_proxy_model    = nullptr;
  ProteinMatch *_p_protein_match;
  ProjectWindow *_project_window;
  IdentificationGroup *_p_identification_group = nullptr;
  QModelIndex m_askIndex;
  QMenu *mp_copyPathMenu = nullptr;
};
