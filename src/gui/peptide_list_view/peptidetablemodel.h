
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QAbstractTableModel>
#include "../../core/project.h"


/** \def PeptideListColumn list of available fields to display in peptide list
 *
 */

enum class PeptideListColumn : std::int8_t
{
  checked             = 0, ///< manual checked
  peptide_grouping_id = 1, ///< manual checked
  engine              = 2,
  sample              = 3,
  scan                = 4,
  spectrum_index      = 5, ///< spectrum index
  rtmin               = 6,
  rt                  = 7,
  ion_mobility_begin  = 8,
  ion_mobility_end    = 9,
  collision_energy    = 10,
  charge              = 11,
  experimental_mz     = 12,
  sequence_nter       = 13,
  sequence            = 14,
  sequence_cter       = 15,
  modifs              = 16,
  label               = 17,
  start               = 18,
  length              = 19,
  used                = 20,
  subgroups           = 21,
  Evalue              = 22,
  qvalue              = 23,
  experimental_mhplus = 24,
  theoretical_mhplus  = 25,
  delta_mhplus        = 26,
  delta_ppm           = 27,


  peptide_prophet_probability       = 28, ///< no PSI MS description
  peptide_inter_prophet_probability = 29, ///< no PSI MS description
  tandem_hyperscore                 = 30, ///< X!Tandem hyperscore MS:1001331
  mascot_score = 31, ///< PSI-MS MS:1001171 mascot:score 56.16
  mascot_expectation_value =
    32, ///< PSI-MS MS:1001172 mascot:expectation value 2.42102904673618e-006
  omssa_evalue        = 33, ///< MS:1001328  "OMSSA E-value." [PSI:PI]
  omssa_pvalue        = 34, ///< MS:1001329  "OMSSA p-value." [PSI:PI]
  msgfplus_raw        = 35, ///< MS:1002049  "MS-GF raw score." [PSI:PI]
  msgfplus_denovo     = 36, ///< MS:1002050  "MS-GF de novo score." [PSI:PI]
  msgfplus_energy     = 37, ///< MS:1002051  "MS-GF energy score." [PSI:PI]
  msgfplus_SpecEValue = 38, ///< MS:1002052  "MS-GF spectral E-value." [PSI:PI]
  msgfplus_EValue     = 39, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  msgfplus_isotope_error = 40, ///< MS:1002053  "MS-GF E-value." [PSI:PI]
  comet_xcorr   = 41, ///< MS:1002252  "The Comet result 'XCorr'." [PSI:PI]
  comet_deltacn = 42, ///< MS:1002253  "The Comet result 'DeltaCn'." [PSI:PI]
  comet_deltacnstar =
    43, ///< MS:1002254  "The Comet result 'DeltaCnStar'." [PSI:PI]
  comet_spscore = 44, ///< MS:1002255  "The Comet result 'SpScore'." [PSI:PI]
  comet_sprank  = 45, ///< MS:1002256  "The Comet result 'SpRank'." [PSI:PI]
  comet_expectation_value =
    46, ///< MS:1002257  "The Comet result 'Expectation value'." [PSI:PI]
  deepprot_original_count  = 47, ///< number of matched peaks before specfit
  deepprot_fitted_count    = 48, ///< number of matched peaks after specfit
  deepprot_match_type      = 49, ///< DeepProt spectrum match type
  deepprot_status          = 50, ///< DeepProt PSM status
  deepprot_mass_delta      = 51, ///< DeepProt mass delta
  deepprot_delta_positions = 52, ///< DeepProt mass delta positions
};

class PeptideListWindow;

class PeptideTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PeptideTableModel(PeptideListWindow *parent);
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  static const QString getTitle(PeptideListColumn column);
  static const QString getDescription(PeptideListColumn column);
  static PeptideListColumn getPeptideListColumn(std::int8_t column);
  bool hasColumn(PeptideListColumn column);

  void setProteinMatch(ProteinMatch *p_protein_match);
  ProteinMatch *getProteinMatch();
  signals:
  void peptideEvidenceClicked(PeptideEvidence *p_peptide_evidence);

  public slots:
  void onPeptideDataChanged();

  private:
  static const QString getTitle(std::int8_t column);
  static const QString getDescription(std::int8_t column);
  static int getColumnWidth(int column);

  private:
  const std::int8_t m_nonIdentificationEngineColumnLimit = 28;
  ProteinMatch *_p_protein_match                         = nullptr;
  PeptideListWindow *_p_peptide_list_window              = nullptr;
  // contains columns to display (present in this peptide match)
  std::set<PeptideListColumn> _engine_columns_to_display;
};
