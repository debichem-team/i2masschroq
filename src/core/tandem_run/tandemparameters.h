/**
 * \file /core/tandem_run/tandemparameters.h
 * \date 19/9/2017
 * \author Olivier Langella
 * \brief handles X!Tandem parameters
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QString>
#include <QMap>

class TandemParameters
{
  public:
  TandemParameters();
  TandemParameters(const TandemParameters &other);
  TandemParameters &operator=(const TandemParameters &);
  virtual ~TandemParameters();

  const QString getValue(const QString &label) const;
  void setMethodName(const QString &method);
  const QString &getMethodName() const;
  void setParamLabelValue(const QString &label, const QString &value);
  const QMap<QString, QString> &getMapLabelValue() const;
  bool equals(const TandemParameters &other) const;

  /** @brief reset tandem parameters
   * */
  void clear();

  private:
  const QString getLabelCategory(const QString &value) const;

  private:
  QString _method_name;
  QMap<QString, QString> _map_label_value;
};
