/**
 * \file src/core/condor_process/condorqueueparser.h
 * \date 24/01/2022
 * \author Olivier Langella
 * \brief parse condor_q --xml command results
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../../utils/types.h"


/**
 * @todo write docs
 */
class CondorQueueParser : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  CondorQueueParser();

  /**
   * Destructor
   */
  virtual ~CondorQueueParser();

  /** @brief count current HTCondor job
   * @param status condor job status to count
   */
  std::size_t countCondorJobStatus(CondorJobStatus status) const;

  /** @brief set the total number of jobs awaited
   * @param total the number of jobs
   */
  void setCondorJobSize(std::size_t total);

  protected:
  virtual void readStream();

  private:
  void read_a();
  void end_c();

  private:
      std::size_t m_totalJobs =0;
  bool m_isEmpty = true;
  std::size_t m_countStatus[10];
  int m_currentProcId;
  CondorJobStatus m_currentCondorJobStatus;
  QString m_currentRemoteHost;
  QString m_currentLastRemoteHost;
};
