/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include <QDebug>
#include <memory>
#include "msrun.h"
#include "../gui/xic_view/xic_widgets/zivywidget.h"
#include "masschroq_run/masschroqfileparameters.h"

enum class AlignmentGroupStatus : std::int8_t
{
  created             = 0, ///< Group created
  masschroqml_written = 1, ///< Group created and masschroqml written
  masschroq_run =
    2, ///< Group created, masschroqml written and result file produced
  rdata_written = 3, ///< Group created, masschroqml written, result file
                     ///< produced and MCQR RData written
};

class MsRunAlignmentGroup;
typedef std::shared_ptr<MsRunAlignmentGroup> MsRunAlignmentGroupSp;

class Project;
class MsRunAlignmentGroup
{
  public:
  MsRunAlignmentGroup(Project *project, QString group_name);
  ~MsRunAlignmentGroup();

  void setMsRunAlignmentGroupName(QString group_name);
  const QString &getMsRunAlignmentGroupName() const;

  void setMsRunReference(MsRunSp reference);
  MsRunSp getMsRunReference() const;

  const std::vector<MsRunSp> &getMsRunsInAlignmentGroup() const;

  void addMsRunToMsRunAlignmentGroupList(MsRunSp new_msrun);
  void removeMsRunFromMsRunAlignmentGroupList(MsRunSp removed_msrun);
  void prepareMsrunRetentionTimesForAlignment() const;

  void savePostMassChroqmlParameters(QString masschroqml_file);
  void setMassChroQRunStatus(AlignmentGroupStatus status);
  void setMassChroQRunStatusFromInt(int status);
  AlignmentGroupStatus getGroupStatus();
  const QString &getMassChroqmlPath() const;
  void setMassChroqmlPath(QString masschroqml_path);

  const QString getMassChroqmlRdataPath() const;
  const QString getMassChroqmlResultTsvDirectoryPath() const;
  const QString getMassChroqmlResultProteinListPath() const;
  const QString getMassChroqmlResultPeptideListPath() const;

  private:
  QString m_groupName;
  Project *mp_project;
  std::vector<MsRunSp> m_msrunAlignmentGroupList;
  MsRunSp msp_msRunReference = nullptr;

  // Post MassChroQ run params
  AlignmentGroupStatus m_status;
  MasschroqFileParametersSp msp_masschroqFileParametersSp;
  QString m_masschroqmlPath;
};
