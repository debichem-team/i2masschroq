
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once
#include "identificationdatasource.h"
#include <QFileInfo>

class IdentificationXtandemFile : public IdentificationDataSource
{
  public:
  IdentificationXtandemFile(const QFileInfo &xtandem_file);
  IdentificationXtandemFile(const IdentificationXtandemFile &other);
  ~IdentificationXtandemFile();

  virtual void parseTo(Project *p_project) override;


  // virtual MsRunSp getMsRunSp() const override;
  void setTimstofMs2CentroidParameters(const QString &centroid_parameters);

  private:
  const QFileInfo _xtandem_file;
  QString m_centroidOptions;
};
