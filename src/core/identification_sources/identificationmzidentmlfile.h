/**
 * \file src/core/identification_sources/identificationmzidentmlfile.h
 * \date 20/1/2021
 * \author Olivier Langella
 * \brief parse mzIdentML file
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once
#include "identificationdatasource.h"
#include <QFileInfo>

class IdentificationMzIdentMlFile : public IdentificationDataSource
{
  public:
  IdentificationMzIdentMlFile(const QFileInfo &xtandem_file);
  IdentificationMzIdentMlFile(const IdentificationMzIdentMlFile &other);
  ~IdentificationMzIdentMlFile();

  virtual void parseTo(Project *p_project) override;


  private:
  /** @brief before parsing the file, we need to know on which mz raw data file
   * the identification was made
   */
  void getMzIdentMlFileMsRunSp(Project *p_project);

  private:
  const QFileInfo m_mzidentmlFile;
};
