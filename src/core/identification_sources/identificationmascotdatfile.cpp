/**
 * \file /core/identification_sources/identificationmascotdatfile.cpp
 * \date 17/2/2018
 * \author Olivier Langella
 * \brief mascot dat identification file handler
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "identificationmascotdatfile.h"

#include <pappsomspp/pappsoexception.h>
#include "../project.h"
#include "../../input/mascot/mascotdatparser.h"

IdentificationMascotDatFile::IdentificationMascotDatFile(
  const QFileInfo &mascot_dat_file)
  : IdentificationDataSource(mascot_dat_file.absoluteFilePath()),
    _mascot_dat_file(mascot_dat_file)
{
  m_identificationEngine = IdentificationEngine::mascot;
}

IdentificationMascotDatFile::IdentificationMascotDatFile(
  const IdentificationMascotDatFile &other)
  : IdentificationDataSource(other), _mascot_dat_file(other._mascot_dat_file)
{
  m_identificationEngine = IdentificationEngine::mascot;
}

IdentificationMascotDatFile::~IdentificationMascotDatFile()
{
}


void
IdentificationMascotDatFile::parseTo(Project *p_project)
{
  qDebug() << "IdentificationMascotDatFile::parseTo begin";

  qDebug() << "Read Mascot dat result file '"
           << _mascot_dat_file.absoluteFilePath() << "'";

  MsRunSp msrun_sp = p_project->getMsRunStore().getInstance(
    QFileInfo(_mascot_dat_file).baseName());
  setMsRunSp(msrun_sp);
  std::vector<IdentificationGroup *> identification_list =
    p_project->getIdentificationGroupList();
  IdentificationGroup *identification_group_p = nullptr;
  if(p_project->getProjectMode() == ProjectMode::combined)
    {
      if(identification_list.size() == 0)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
      else
        {
          identification_group_p = identification_list[0];
        }
    }
  else
    {
      for(IdentificationGroup *identification_p_flist : identification_list)
        {
          if(identification_p_flist->containSample(
               msrun_sp.get()->getSampleName()))
            {
              identification_group_p = identification_p_flist;
              break;
            }
        }
      if(identification_group_p == nullptr)
        {
          identification_group_p = p_project->newIdentificationGroup();
        }
    }

  identification_group_p->addIdentificationDataSourceP(this);
  MascotDatParser mascot_parser(p_project, identification_group_p, this);

  QFile qfile(_mascot_dat_file.absoluteFilePath());
  mascot_parser.parse(&qfile);

  qfile.close();

  qDebug() << "IdentificationMascotDatFile::parseTo end";
}

/*
const bool IdentificationMascotDatFile::isValid(const PeptideEvidence *
p_peptide_evidence, const AutomaticFilterParameters &
automatic_filter_parameters) const {

       return true;
}
*/
