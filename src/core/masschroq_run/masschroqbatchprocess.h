/**
 * \file /core/masschroq_run/masschroqbatchprocess.h
 * \date 2/11/2020
 * \author Thomas Renne
 * \brief handles execution of a bunch of MassChroQ process
 */

/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include "masschroqrunbatch.h"
#include "../../gui/mainwindow.h"
#include "../../utils/workmonitor.h"

class MassChroQBatchProcess : public QObject
{
  Q_OBJECT
  public:
  MassChroQBatchProcess(MainWindow *p_main_window,
                        WorkMonitorInterface *p_monitor,
                        const MassChroQRunBatch masschroq_batch_param);
  virtual ~MassChroQBatchProcess();
  virtual void run();

  protected:
  void checkMassChroQRunBatch();
  void checkMassChroQMLValidity();

  protected:
  MassChroQRunBatch m_masschroqRunBatch;
  WorkMonitorInterface *mp_monitor;


  private slots:
  void readyReadStandardOutput();
  void readyReadStandardError();

  private:
  MainWindow *mp_mainWindow = nullptr;
  bool m_checkMcqTempDir    = false;

  QProcess *mpa_mcqProcess = nullptr;
  QString m_mcqErrorString;
};
