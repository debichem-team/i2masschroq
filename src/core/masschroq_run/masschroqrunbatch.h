/**
 * \file /core/masschroq_run/masschroqrunbatch.h
 * \date 2/11/2020
 * \author Thomas Renne
 * \brief all data needed to run a MassChroQ batch
 */

/*******************************************************************************
 * Copyright (c) 2020 Thomas Renne <thomas.renne@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *

 * Contributors:
 *     Thomas Renne <thomas.renne@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QString>


/** @brief MassChroQ execution parameters
 */
struct MassChroQRunBatch
{
  QString masschroq_bin_path;
  QString masschroq_temporary_dir_path;
  QString masschroqml_path;
  std::size_t number_cpu = 1;
  bool on_disk           = false;
  bool parse_peptide     = false;
};
