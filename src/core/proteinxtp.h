
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <pappsomspp/protein/protein.h>
#include <pappsomspp/types.h>
#include <list>
#include <QUrl>
#include "sequencedatabase.h"
#include "../utils/types.h"

class FastaFile;

#pragma once

struct DbXref
{
  DbXref();
  DbXref(const DbXref &other);
  DbXref(ExternalDatabase database_in, const QString &accession_in);
  DbXref &operator=(const DbXref &);
  const QUrl getUrl() const;
  ExternalDatabase database;
  QString accession;
};

class ProteinXtp;

/** \brief shared pointer on a Protein object
 */
typedef std::shared_ptr<ProteinXtp> ProteinXtpSp;

class ProteinXtp : public pappso::Protein
{
  public:
  ProteinXtp();
  ProteinXtp(const ProteinXtp &other);
  virtual ~ProteinXtp();
  bool operator==(const ProteinXtp &other) const;


  ProteinXtpSp makeProteinXtpSp() const;

  /** @brief separates accession from description based on first space separator
   */
  void setCompleteDescription(const QString &full_description);

  void setIsContaminant(bool conta);
  void setIsDecoy(bool conta);
  void setFastaFileP(const FastaFile *fasta_file);
  const FastaFile *getFastaFileP() const;
  bool isContaminant() const;
  bool isDecoy() const;
  std::size_t countTrypticPeptidesForPAI() const;

  /** @brief get only amino acid sequence
   * clean sequence of other characters : * _ ...
   */
  QString getOnlyAminoAcidSequence() const;

  /** @brief get protein mass in dalton
   */
  pappso::pappso_double getMass() const;

  /** @brief look for external database accessions in accesion text
   */
  void parseAccession2dbxref();

  const std::list<DbXref> &getDbxrefList() const;

  private:
  SequenceDatabase *_p_sequence_database;
  bool _is_decoy       = false;
  bool _is_contaminant = false;
  std::list<DbXref> _dbxref_list;

  const FastaFile *_p_fasta = nullptr;
};
