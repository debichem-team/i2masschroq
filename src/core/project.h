/**
 * \file src/core/mzidentml/project.h
 * \date 14/11/2015
 * \author Olivier Langella
 * \brief central object to handle all data
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once

#include <memory>
#include "identificationgroup.h"
#include "automaticfilterparameters.h"
#include "../utils/types.h"
#include "../utils/peptidestore.h"
#include "../utils/proteinstore.h"
#include "../utils/identificationdatasourcestore.h"
#include "../utils/msrunstore.h"
#include "../utils/workmonitor.h"
#include "labeling/labelingmethod.h"
#include "alignmentgroup.h"
#include "mcqr/mcqrexperiment.h"
#include "masschroq_run/masschroqfileparameters.h"

class Project;
typedef std::shared_ptr<Project> ProjectSp;

class PeptideMatch;
class ProteinMatch;


/** @brief a project contains all information of a proteomics experiment
 */
class Project
{
  public:
  Project();
  ~Project();

  /** @brief make a shared pointer on project object
   * any reference on a project should use a shared pointer
   * @return ProjectSp
   */
  ProjectSp makeProjectSp() const;

  /** @brief get the protein store object
   * the protein store object guarantees the unicity of proteins in the project
   * scope
   * @return ProteinStore
   */
  ProteinStore &getProteinStore();

  /** @brief get the peptide store object
   * the peptide store object guarantees the unicity of peptides in the project
   * scope
   * @return PeptideStore
   */
  PeptideStore &getPeptideStore();


  /** @brief get a constant protein store object
   * the protein store object guarantees the unicity of proteins in the project
   * scope
   * @return const ProteinStore
   */
  const ProteinStore &getProteinStore() const;


  /** @brief get the msrun store object
   * the msrun store object guarantees the unicity of msruns in the project
   * scope
   * @return MsRunStore
   */
  MsRunStore &getMsRunStore();

  /** @brief get a constant msrun store object
   * the msrun store object guarantees the unicity of msruns in the project
   * scope
   * @return const MsRunStore
   */
  const MsRunStore &getMsRunStore() const;

  /** @brief get the fasta file store object
   * the fasta file store object guarantees the unicity of fasta files in the
   * project scope
   * @return FastaFileStore
   */
  FastaFileStore &getFastaFileStore();

  /** @brief get a constant fasta file store object
   * the fasta file store object guarantees the unicity of fasta files in the
   * project scope
   * @return const FastaFileStore
   */
  const FastaFileStore &getFastaFileStore() const;


  IdentificationDataSourceStore &getIdentificationDataSourceStore();
  const IdentificationDataSourceStore &getIdentificationDataSourceStore() const;
  void readXpipFile(WorkMonitorInterface *p_monitor, QFileInfo xpip_source);
  IdentificationGroup *newIdentificationGroup();

  /** @brief validate or invalidate peptides and proteins based automatic
   * filters and manual checks
   * */
  void updateAutomaticFilters(
    const AutomaticFilterParameters &automatic_filter_parameters);
  const AutomaticFilterParameters &getAutomaticFilterParameters() const;
  void startGrouping(WorkMonitorInterface *p_work_monitor);

  /** @brief get the grouping type experiment for this project
   *
   * @return GroupingType enum peptide mass, phospho, or old java algortithm
   */
  GroupingType getGroupingType() const;

  /** @brief get the contaminant removal mode used in the project
   *
   * @return ContaminantRemovalMode, see enum description
   */
  ContaminantRemovalMode getContaminantRemovalMode() const;

  /** @brief set the contaminant removal mode used in the project
   *
   * @param ContaminantRemovalMode see enum description
   */
  void
  setContaminantRemovalMode(ContaminantRemovalMode contaminant_removal_mode);

  /** @brief get the project mode
   * @return ProjectMode individual or combined
   */
  ProjectMode getProjectMode() const;

  /** @brief set the project mode
   * @param ProjectMode individual or combined
   */
  void setProjectMode(ProjectMode mode);

  /** @brief give a name to the project
   *
   * @param name no restriction in the project name
   */
  void setProjectName(const QString &name);

  /** @brief get name to the project
   *
   * @return QString
   */
  QString getProjectName();

  /** @brief Handle the MsRun Alignment groups for MassChroQ
   */
  std::vector<MsRunAlignmentGroupSp> getMsRunAlignmentGroupList();
  MsRunAlignmentGroupSp getMsRunAlignmentGroupFromList(QString group_name);
  void addMsRunAlignmentGroupToList(MsRunAlignmentGroupSp new_group);

  /** @brief Handle the MsRun Alignment group with all the MsRuns inside
   */
  MsRunAlignmentGroupSp getAllMsRunAlignmentGroup();
  void setAllMsRunAlignmentGroup(MsRunAlignmentGroupSp all_msrun_grouped);

  std::vector<IdentificationGroup *> getIdentificationGroupList();
  const std::vector<IdentificationGroup *> getIdentificationGroupList() const;
  void readResultFile(QString filename);

  /** @brief check that modifications are coded with PSI MOD accessions
   */
  bool checkPsimodCompliance() const;

  /** @brief apply labeling method to all peptide match
   * */
  void setLabelingMethodSp(LabelingMethodSp labeling_method_sp);

  /** @brief get labeling method shared pointer
   * @return LabelingMethodSp
   * */
  LabelingMethodSp getLabelingMethodSp() const;

  /** @brief tells if grouping has been made on PTM sites (phosphorylation or
   * othe PTM)
   * @return bool
   */
  bool hasPtmExperiment() const;

  /** @brief look for a peptide in the same XIC
   * @param peptide_evidence_list the peptide evidence list to build
   * @param p_msrun MSrun to look for
   * @param p_peptide peptide to look for
   * @param charge charge to look for
   */
  void getSameXicPeptideEvidenceList(
    std::vector<const PeptideEvidence *> &peptide_evidence_list,
    const MsRun *p_msrun,
    const PeptideXtp *p_peptide,
    unsigned int charge) const;


  void prepareMsrunRetentionTimesForAlignment() const;

  /** @brief handle project status (data modifed or not)
   */
  void setProjectChangedStatus(bool updated_data);
  bool getProjectChangedStatus() const;

  /** @brief handle McqrExperiment
   */
  void setMcqrExperimentSp(const McqrExperimentSp &mcqr_exp);
  const McqrExperimentSp &getMcqrExperimentSp() const;

  /** @brief get the PTM mode
   * @return PtmMode see enumeration description
   */
  PtmMode getPtmMode() const;

  /** @brief set the PTM mode
   * @param mode see enumeration description
   */
  void setPtmMode(PtmMode mode);

  /** @brief tells if a peptide evidence is related to a target protein
   */
  bool isTarget(PeptideEvidence *p_pe) const;

  /** @brief get MassChroQ parameters specific to this project
   */
  const MasschroqFileParametersSp &getMasschroqFileParametersSp() const;

  private:
  LabelingMethodSp _labeling_method_sp;
  ProjectMode _project_mode = ProjectMode::combined;
  PtmMode m_ptmMode         = PtmMode::none;
  std::vector<IdentificationGroup *> _identification_goup_list;
  std::vector<IdentificationDataSourceSp> m_identificationDataSourceSpList;
  std::vector<MsRunAlignmentGroupSp> m_msRunAlignmentGroupList;
  MsRunAlignmentGroupSp m_allMsRunAlignmentGroup       = nullptr;
  IdentificationGroup *_p_current_identification_group = nullptr;

  AutomaticFilterParameters _automatic_filter_parameters;

  GroupingType m_groupingType = GroupingType::PeptideMass;
  ContaminantRemovalMode m_contaminantRemovalMode =
    ContaminantRemovalMode::groups;

  McqrExperimentSp msp_mcqrExperiment;
  QString m_project_name = "new_project";
  ProteinStore _protein_store;
  PeptideStore _peptide_store;
  IdentificationDataSourceStore _identification_data_source_store;
  MsRunStore _msrun_store;
  FastaFileStore _fasta_file_store;
  bool m_project_changed = false;
  MasschroqFileParametersSp msp_masschroqFileParametersSp;
};
