/**
 * \file core/mcqr/mcqrexperiment.h
 * \date 13/01/2021
 * \author Thomas Renne
 * \brief handle mcqr experiment
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include "mcqrmetadata.h"
#include "mcqrexpsummary.h"
#include "../alignmentgroup.h"


class McqrExperiment;
typedef std::shared_ptr<McqrExperiment> McqrExperimentSp;

class McqrExperiment
{
  public:
  McqrExperiment();
  ~McqrExperiment();

  void addColumnName(QString col_name);
  void addNewMcqrMetadataLine(const McqrMetadata &metadata_line);

  const std::vector<McqrMetadata> &getmetadataInfoVector() const;
  const std::vector<QString> &getmetadataColumns() const;

  void clearMetadataInfoAndColumnsVectors();

  void setMcqrExpSummary(McqrExpSummary summary);
  McqrExpSummary getMcqrExpSummary();

  void
  setAlignmentGroupLinkedToExperiment(MsRunAlignmentGroupSp alignment_group);
  MsRunAlignmentGroupSp getAlignmentGroupLinkedToExperiment();

  McqrLoadDataMode getMcqrLoadDataMode() const;

  void setMcqrWorkingDirectory(const QString &wd);
  const QString &getMcqrWorkingDirectory() const;

  void setRdataFilePath(const QString &rdata_file);
  const QString &getRdataFilePath() const;

  void setMetadataOdsFilePath(const QString &metadata_file);
  const QString &getMetadataOdsFilePath() const;

  QString getMassChroqMlFilePath() const;


  McqrExperimentType getMcqrExperimentType() const;
  void setMcqrExperimentType(McqrExperimentType exp_type);

  private:
  std::vector<McqrMetadata> m_metadataList;
  std::vector<QString> m_metadataColumns;
  McqrExpSummary m_experimentalSummary;
  MsRunAlignmentGroupSp msp_alignmentGroup;

  QString m_rdata_absolute_path;
  QString m_metadataOdsFilePath;
  QString m_mcqrWorkingDirectoryPath;
  McqrExperimentType m_mcqrExperimentType;
};
