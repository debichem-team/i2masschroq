/**
 * \file core/mcqr/mcqrexperiment.cpp
 * \date 13/01/2021
 * \author Thomas Renne
 * \brief handle mcqr experiment
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#include "mcqrexperiment.h"
#include <pappsomspp/pappsoexception.h>

McqrExperiment::McqrExperiment()
{
}

McqrExperiment::~McqrExperiment()
{
}

void
McqrExperiment::addColumnName(QString col_name)
{
  qDebug() << col_name;
  m_metadataColumns.push_back(col_name);
}

void
McqrExperiment::addNewMcqrMetadataLine(const McqrMetadata &metadata_line)
{
  qDebug() << metadata_line.m_otherData.size();
  // check
  // check column names :
  for(std::pair<QString, QVariant> pair_name : metadata_line.m_otherData)
    {
      if(std::find(m_metadataColumns.begin(),
                   m_metadataColumns.end(),
                   pair_name.first) == m_metadataColumns.end())
        {
          throw pappso::PappsoException(
            QObject::tr("column name %1 not defined in McqrExperiment")
              .arg(pair_name.first));
        }
    }
  for(const QString &column_name : m_metadataColumns)
    {
      if((column_name == "msrun") || (column_name == "msrunfile"))
        {
        }
      else
        {
          if(metadata_line.m_otherData.find(column_name) ==
             metadata_line.m_otherData.end())
            {
              throw pappso::PappsoException(
                QObject::tr("column name %1 not defined in metadata_line")
                  .arg(column_name));
            }
        }
    }
  m_metadataList.push_back(metadata_line);
}

const std::vector<McqrMetadata> &
McqrExperiment::getmetadataInfoVector() const
{
  return m_metadataList;
}

const std::vector<QString> &
McqrExperiment::getmetadataColumns() const
{
  return m_metadataColumns;
}

McqrExpSummary
McqrExperiment::getMcqrExpSummary()
{
  return m_experimentalSummary;
}

void
McqrExperiment::setMcqrExpSummary(McqrExpSummary summary)
{
  m_experimentalSummary = summary;
}

MsRunAlignmentGroupSp
McqrExperiment::getAlignmentGroupLinkedToExperiment()
{
  return msp_alignmentGroup;
}

void
McqrExperiment::setAlignmentGroupLinkedToExperiment(
  MsRunAlignmentGroupSp alignment_group)
{
  msp_alignmentGroup = alignment_group;
}

void
McqrExperiment::clearMetadataInfoAndColumnsVectors()
{
  m_metadataList.clear();
  m_metadataColumns.clear();
}

McqrLoadDataMode
McqrExperiment::getMcqrLoadDataMode() const
{
  McqrLoadDataMode mode = McqrLoadDataMode::basic;

  if(m_experimentalSummary.m_fractioning)
    {
      mode = McqrLoadDataMode::fraction;
    }
  if(m_experimentalSummary.m_isotopicLabeling)
    {
      if(mode == McqrLoadDataMode::fraction)
        {
          mode = McqrLoadDataMode::both;
        }
      else
        {
          mode = McqrLoadDataMode::label;
        }
    }
  return mode;
}

void
McqrExperiment::setRdataFilePath(const QString &rdata_file)
{
  qDebug() << rdata_file;
  m_rdata_absolute_path = rdata_file;
}

const QString &
McqrExperiment::getRdataFilePath() const
{
  qDebug() << m_rdata_absolute_path;
  return m_rdata_absolute_path;
}

QString
McqrExperiment::getMassChroqMlFilePath() const
{
  if(msp_alignmentGroup != nullptr)
    {
      return msp_alignmentGroup.get()->getMassChroqmlPath();
    }
  return "";
}

void
McqrExperiment::setMetadataOdsFilePath(const QString &metadata_file)
{
  m_metadataOdsFilePath = metadata_file;
}

const QString &
McqrExperiment::getMetadataOdsFilePath() const
{
  return m_metadataOdsFilePath;
}

void
McqrExperiment::setMcqrWorkingDirectory(const QString &wd)
{
  m_mcqrWorkingDirectoryPath = wd;
}

const QString &
McqrExperiment::getMcqrWorkingDirectory() const
{
  return m_mcqrWorkingDirectoryPath;
}

McqrExperimentType
McqrExperiment::getMcqrExperimentType() const
{
  return m_mcqrExperimentType;
}

void
McqrExperiment::setMcqrExperimentType(McqrExperimentType exp_type)
{
  m_mcqrExperimentType = exp_type;
}
