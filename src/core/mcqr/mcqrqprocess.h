/**
 * \file gui/mcqr_run_view/mcqrqprocess.h
 * \date 12/07/2021
 * \author Thomas Renne
 * \brief Overload the Qprocess for the MCQR process
 */

/*******************************************************************************
 * Copyright (c) 2021 Thomas Renne <thomas.renne@e.email>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Thomas Renne <thomas.renne@e.email> - initial API and
 *implementation
 ******************************************************************************/

#pragma once
#include <QProcess>
#include <QTextStream>

class McqrQProcess : public QProcess
{
  Q_OBJECT
  public:
  McqrQProcess(QObject *parent = nullptr);
  ~McqrQProcess();
  void startRprocess();
  void stopRprocess();

  qint64 executeOnTheRecord(const QString &mcqr_code);
  qint64 executeOffTheRecord(const QString &mcqr_otr);
  void executeMcqrStep(const QString &mcqr_step_name, const QString &mcqr_step);
  const QString &getRscriptCode() const;


  private:
  QString m_rscriptCode;
  QTextStream m_rscriptTextStream;
};
