/**
 * \file core/mcqr/mcqrmetadata.cpp
 * \date 8/4/2022
 * \author Olivier Langella
 * \brief Store metadata lines
 */
/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************/
#include "mcqrmetadata.h"
#include <pappsomspp/exception/exceptionnotfound.h>

const QVariant &
McqrMetadata::getQVariantByColumnName(const QString &name) const
{
  std::map<QString, QVariant>::const_iterator ret = m_otherData.find(name);
  if(ret == m_otherData.end())
    {
      throw pappso::ExceptionNotFound(
        QObject::tr("column name %1 not found in metadata").arg(name));
    }
  return ret->second;
};
