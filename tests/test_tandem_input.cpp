/**
 * \file tests/test_tandem_input.cpp
 * \date 5/12/2021
 * \author Olivier Langella
 * \brief test tandem XML parser
 */


/*******************************************************************************
 * Copyright (c) 2021 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// ./tests/catch2-only-tests [tandem] -s
// ./tests/catch2-only-tests [mzidentml] -s

#include <QDebug>
#include <QString>

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do
                          // this in one cpp file
#include <catch2/catch.hpp>
#include "../src/input/mzidentml/msrunsaxhandler.h"
#include "../src/input/tandem/tandeminfoparser.h"
#include "../src/input/tandem/tandemparamparser.h"
#include "config.h"


TEST_CASE("mzidentml input test suite", "[mzidentml]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: mzIdentML parser init ::..", "[mzidentml]")
  {
    MsRunSaxHandler *parser = new MsRunSaxHandler();

    QXmlSimpleReader simplereader;
    simplereader.setContentHandler(parser);
    simplereader.setErrorHandler(parser);

    QFile qfile(
      QString("%1/tests/data/input/deepprot_test.mzid").arg(CMAKE_SOURCE_DIR));
    QXmlInputSource xmlInputSource(&qfile);

    if(simplereader.parse(xmlInputSource))
      {
        qDebug() << parser->getSpectraDataLocation();
        REQUIRE(parser->getSpectraDataLocation().toStdString() ==
                "../test/data/"
                "01650b_BG3-TUM_first_pool_65_01_01-DDA-1h-R2-59678.mgf");
        qfile.close();

        delete parser;
      }
    else
      {
        qDebug() << parser->errorString();
        // throw PappsoException(
        //    QObject::tr("error reading tandem XML result file :\n").append(
        //         parser->errorString()));

        qfile.close();

        throw pappso::PappsoException(
          QObject::tr("Error reading %1 mzIdentML file :\n %2")
            .arg("")
            .arg(parser->errorString()));
      }
  }
}

TEST_CASE("tandem input test suite", "[tandem]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: X!Tandem simple parser ::..", "[tandem]")
  {

    TandemInfoParser tandem_info_parser;

    bool read_ok = tandem_info_parser.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/input/20120906_balliau_extract_1_A01_urnb-1.xml"));
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(tandem_info_parser.getSpectraDataLocation().toStdString() ==
            "/media/langella/pappso/donnees/extraction/mzxml/"
            "20120906_balliau_extract_1_A01_urnb-1.mzXML");

    REQUIRE(tandem_info_parser.getModelCount() == 8);
  }


  SECTION("..:: X!Tandem preset parser ::..", "[tandem]")
  {

    TandemParamParser tandem_param_parser;

    bool read_ok = tandem_param_parser.readFile(
      QString(CMAKE_SOURCE_DIR).append("/tests/data/input/timstof_preset.xml"));
    REQUIRE(read_ok);
    // REQUIRE(tandem_info_parser.errorString().toStdString() == "");
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("output, histogram column width")
              .toStdString() == "30");

    REQUIRE(tandem_param_parser.getParamFileType() == ParamFileType::preset);
    REQUIRE(tandem_param_parser.getMethodName().toStdString() == "");
    //"refine, potential modification mass">15.99491@M
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("refine, potential modification mass")
              .toStdString() == "15.99491@M");


    read_ok = tandem_param_parser.readFile(
      QString(CMAKE_SOURCE_DIR)
        .append("/tests/data/input/20120906_balliau_extract_1_A01_urnb-1.xml"));
    REQUIRE(read_ok);


    REQUIRE(tandem_param_parser.getParamFileType() == ParamFileType::result);
    REQUIRE(tandem_param_parser.getMethodName().toStdString() ==
            "QExactive_analysis_FDR_nosemi");
    //"refine, potential modification mass">15.99491@M
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("refine, potential modification mass")
              .toStdString() == "15.99491@M");
    //"spectrum, timstof MS2 centroid parameters">20000 2.0 4 10.0
    REQUIRE(tandem_param_parser.getTandemParameters()
              .getValue("spectrum, timstof MS2 centroid parameters")
              .toStdString() == "20000 2.0 4 10.0");

    //"list path, default parameters"

    REQUIRE_THROWS(tandem_param_parser.getTandemParameters()
                     .getValue("list path, default parameters")
                     .toStdString() == "20000 2.0 4 10.0");
  }
}
